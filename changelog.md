3.6.0
-----
- Afegit un nou tipus de property: **Binary**
  - Aquest tipus de property serveix per guardar dades binàries a la DB o al JSON.

3.5.0
-----
- Afegit un nou helper "importSafeArrayProperty"

3.4.0
-----
- **Canvis importants**
  - Els properties de tipus **IS_OBJECT** ara s'autoinicialitzen en accedir a ells per primera vegada:
    - Si existeix un getter per al property, es farà servir aquest (com sempre)
    - Si es volen properties que son objectes pero que poden ser null, s'han de fer servir el **IS_OBJECT_NULLABLE**
    - Si es vol **desactivar aquesta funcionalitat**, s'ha de configurar (globalment) **Objectum::$CreateObjectPropertiesOnGet = false** 

3.3.0
-----
- **Canvis importants**
  - El mètode Serializable::json ara té un paràmetre més per definir si el filtre és per excloure o incloure els fields
  - Eliminat el mètode Serializable::jsonFiltered (ara es fa servir el json normal)

3.2.0
-----
- Implementada una versió molt més optimitzada de "jsonFiltered"
  - **Atenció** Internament ha canviat la signatura de **exportCustomProperty**

3.1.0
-----
- Afegit un nou mètode **Serializable::jsonFiltered(array $fields)**
  - Aquest mètode retorna l'objecte en format JSON i retorna només aquells properties definits dins del $fields

3.0.0
-----
- **Canvis important** 
  - Serializable::$ivar_publicId: Ara és **static**
  - Serializable::$ivar_internalId: Ara és **static** 
  - idToRawData($id): Ara és **final**
    - Per override: static __idToRawData($id)
  - idFromRawData($id): Ara és **final**
	  - Per override: static __idFromRawData($id)
  - idToRawData($id): Ara és **final**
    - Per override: static __idToRawData($id)
  - dateTimeToRawData(\DateTime $dateTime): Ara és **final**
    - Per override: static __dateTimeToRawData(\DateTime $dateTime)
  - dateTimeFromRawData($dateTime): DateTime: Ara és **final**
    - Per override: static __dateTimeFromRawData($dateTime): DateTime
  - isRawDateTimeInstance($value): bool: Ara és **final**	
    - Per override: static __isRawDateTimeInstance($value): bool
  - binaryDataToRawData($data): Ara és **final**
    - Per override: static __binaryDataToRawData($data)
  - binaryDataFromRawData($data): Ara és **final**
    - Per override: static __binaryDataFromRawData($data)
  - isBinaryDataInstance($value): bool: Ara és **final**
    - Per override: static __isBinaryDataInstance($value): bool 

2.8.0
-----
- Millorades algunes validacions (set_ivar)
- Implementat un mètode "parseDataBeforeInsertInIvarsOnDemand" que s'utilitza just abans d'afegir informació al "ivarsOnDemand"

2.7.3
-----
- Solucionat bug en el moment d'intentar encriptar un string que és un NULL

2.7.2
-----
- Solucionat bug al generar l'excepció "SerializeUnknownHowToUnserializePropertyException"

2.7.1
-----
- Solucionat bug al convertir un timestamp a DateTime

2.7.0
-----
- Primers canvis per donar suport a PHP 8.0 o superior

2.6.4
-----
- Afegit "si" com a valor TRUE als booleans (set_ivar)

2.6.3
-----
- Solucionat bug a l'importar objectes (Serializable) quan és en mode "replace = true"
  - Els properties es sobreescrivien tot i tenir el "replace = false" 

2.6.2
-----
- Solucionat bug en assignar un property que és "onDemand" (Serializable)

2.6.1
-----
- Millorada la serialització dels properties "MIXED"
	- Ara en el cas que sigui un Array intenta serialitzar els items que hi ha dins si son objectes del tipus **Serializable**

2.6.0
-----
- Canviada la forma de processar les excepcions dels properties al importar i exportar. La forma de fer-ho ara és més personalitzable.
	- Abans:
	 	- **DEPRECATED**: protected function **serializingSpecialProperty**(string $property, $value, bool $rawDataMode)
		- **DEPRECATED**: protected function **unserializingSpecialProperty**(string $property, $value, bool $rawDataMode)
		- **DEPRECATED**: protected function **propertySerializeExceptions**(): void
	- Ara:
		- protected function **exportCustomProperty**(string $property, $value, bool $rawDataMode, ?array $excluded = null, bool $ignoreEmptyValues = true)
		- protected function **importCustomProperty**(string $property, $value, bool $replace = true, bool $rawDataMode = false, bool $writableOnly = false)
		- protected function **customProperties**(): void

2.5.1
-----
- Solucionat bug en clonar un objecte.
	- Ara es tenen en compte els "properties" i "ivars" i es fa un check.

2.5.0
-----
- Canviada la signatura del mètode **instanceByRawDataId($id)**:
	- Abans era: protected static function instanceByRawDataId($id): self
	- Ara és: protected static function instanceByRawDataId($id)

2.4.1
-----
- En el DateFormatter ara és possible configurar el TimeZone:
    - **DateFormatter::$TimeZone** = 'TimeZone_ID'

2.3.2
-----
- Solucionat bug al evaluar els nested properties, ara és possible evaluar objectes i propietats que no heredin de Objectum.

2.3.1
-----
- Solucionat petit bug al intentar obtenir un valor getNestedPropertyValue() quan el valor era "false" o "0" es considerava que no tenia valor. 

2.3.0
-----
- Afegits dos nous mètodes màgics als properties **IS\_BOOLEAN**:
	- **yes**PropertyName() que verifica que el valor és **true** (és un alias als mètodes **is**PropertyName(), es a dir, fan exactament el mateix però millora la lectura del codi en alguns casos).
	- **no**PropertyName() que verifica que el valor és **false**, es a dir, fa el contrari que el mètode **is**PropertyName().

2.2.3
-----
- Millorada la deserialització dels objectes (ara comprova que la informació sigui correcte abans d'intentar importar-la i generar un error genèric del PHP).
- Refactoritzades algunes excepcions.

2.2.2
-----
- Afegit l'idioma "English" (en) als formatters:
	- DateFormatter
	- NumberFormatter
	- PluralsFormatter

2.2.1
-----
- Els properties ara son compatibles amb les funcions **isset()** i **empty()**
	- Implementat el magic method **__isset()** als Objectum.

2.2.0
-----
- Afegit un nou tipus de property: **IS\_ENUM\_NULLABLE**
	- Aquest nou property serveix per definir propietats que només poden contenir uns valors predefinits o **NULL**.

2.1.0
-----
- Afegit un nou tipus de property: **IS\_ARRAY\_ENUM**
    - Aquest nou property serveix per definir propietats del tipus ARRAY que només poden contenir uns valors predefinits.
    - Al afegir elements invàlids genera una excepció.
- Afegit un nou mètode "validateValues" als enums, per poder validar més d'un element a la vegada.
- Solucionat bug al importar properties encriptats.

2.0.0
-----
- El projecte passa a ser públic i **Open Source**, accessible des del Composer.
- Separació del nucli monolític de la llibreria, ara és un Framework modular:
    - **Core:** Classes bàsiques per crear models serialitzables.
    - **MongoDB:** Implementació del Driver MongoDB + Serializable Entities preparades per treballar amb el MongoDB.
    - **Helpers:** Funcions d'ajuda per treballar amb Arrays, CSV, DateTime, Emails, HTML, IDs, Telèfons, RegExp, Strings...
- Afegits dos nous packages:
    - **Helpers:** Funcions d'ajuda per treballar amb Arrays, CSV, DateTime, Emails, HTML, IDs, Telèfons, RegExp, Strings...
    - **Templater:** Conjunt de classes per compilar templates simples.
    - **CLI:** 
    - **Emailer:** Conjunt de classes per facilitar l'enviament d'emails (internament utilitza el Swiftmailer) (PENDENT).

1.8.0
-----
- Afegits els properties "relacionats" pels **IS_OBJECT**, **IS_OBJECT_NULLABLE** i **IS_ARRAY**, on el RawData només guarda l'id, i es serialitzen a la hora de carregar-los.
    - CREATE_ON_SERIALIZE_BY_ID: Serialitza l'objecte en el mateix moment que serialitza l'objecte que el conté.
    - CREATE_ON_DEMAND_BY_ID: Serialitza l'objecte només quan es necessita accedir a ell.
- Integració amb la interfície **\Serializable** del core PHP:
    - public function **serialize**($stringify = false)
    	- **IMPORTANT** A diferencia del que s'espera, aquest per defecte retorna un **stdClass** i no un **string**.
    - public function **unserialize**($serialized)
    	- **NOTA** El contingut del $serialized pot ser un **stdClass**, **Array** o **String (json)**. 
- Integració amb la interfície **\JsonSerializable** del core PHP JSON, que permet serialitzar objectes automàticament quan es fa servir el **json_encode()**
    - public function **jsonSerialize**()

1.7.3
-----
- Implementada compatibilitat amb les últimes versions del Mongo (4.2, 4.0.12+, 3.6.14+ i 3.4.23+)

1.7.2
-----
- El mètode hasMethod() dels Objectum ara tenen en compte els magic methods dels properties com el **Str**, **FirstItem**, **LastItem**, etc...

1.7.1
-----
- Afegits tres nous plurals: **second**, **minute** i **hour**
- Afegits tres nous formatters per les dates, la data la mostra "relativa" a ara:
    - RELATIVE_DATE_TIME_STYLE
    - RELATIVE_ONLY_DATE_STYLE
    - RELATIVE_ONLY_TIME_STYLE

1.7.0
-----
- Afegit un nou tipus de property: **IS\_PROPERTY\_ALIAS**
	- Aquest nou property serveix com a "accés directe" a altres properties del propi objecte.
	- També serveix com a alias a elements d'un property del tipus array, ja que internament utilitza el mètode **getNestedPropertyValue()**. 
	- **Exemple 1**: [Property::READ\_ONLY, Property::IS\_PROPERTY\_ALIAS, 'example.prop1']
	- **Exemple 2**: [Property::READ\_ONLY, Property::IS\_PROPERTY\_ALIAS, 'examples.2.prop1']
- Afegida una nova classe serialitzable: **ExplicitPlainEntity**
	- Aquesta classe es el mix entre una **PlainEntity** i una **ExplicitEntity**
	
1.6.9
-----
- Solucionat bug al serialitzar un property que té un stdClass assignat.

1.6.8
-----
- Actualitzat el composer
- Millorada tota la documentació
	- Eliminats els @throws genèrics per evitar la sobresaturació de @throws al fer servir la llibreria

1.6.7
-----
- Solucionat el problema de passar un **null** al importar properties.

1.6.6
-----
- Els properties DICTIONARY ara també (com els ARRAY) retornen un array buit quan es crida el seu getter per primera vegada.
- Implementats dos nous mètodes als Objectums: 
	- hasMethod(method): Comprova que existeixi el mètode dins la classe.
	- evaluateMethod($method): Evalua i executa un mètode i els seus paràmetres.
- Millorat el mètode **getNestedPropertyValue** ara també sap executar mètodes amb paràmetres (bàsics).

1.6.5
-----
- Implementat a les dates un nou estil de format TIME\_DATE (fins ara només hi havia DATE\_TIME, DATE o TIME).

1.6.4
-----
- Millorada la configuració del composer (afegides les extensions requerides)

1.6.3
-----
- Solucionat bug al convertir en milisegons els DateTime (no tenia en compte els milisegons xD)

1.6.2
-----
- Els DateTime ara també es guarden els milisegons

1.6.1
-----
- Implementada integració amb Composer
	- Ara és possible integrar el Foundation usant Composer


1.6.0
-----
- Afegit un nou tipus de property: **IS\_ENUM**
	- Aquest nou property serveix per definir propietats que només poden contenir uns valors predefinits.
	- Tots els properties IS\_ENUM només poden contenir objectes que heredin de **Enum**.
- Afegida la classe **Enum**
	- Aquesta classe ha de ser sempre Abstracte, ja que només serveix per definir les constants (valors) que accepta aquest enum.	
- Afegit el mètode **copy()** a la classe **Serializable**
	- Aquest mètode retorna una còpia recursiva de l'objecte, on tots els seus elements son creats des de zero.
- Afegits dos nous mètodes automàtics als properties **IS\_ARRAY**.
	- property**FirstItem**: Retorna el primer element de l'array.
	- property**LastItem**: Retorna l'últim element de l'array.
- Solucionat bug amb els mètodes automàtics dels properties quan el property era d'un sol caràcter.
- Millorat el **push\_ivar**: Ara si se li pot passar un array i es fa automàticament el merge entre els dos arrays. També valida que els elements de dins l'array que es vol afegir siguin vàlids.


1.5.0
-----
- Afegit un nou tipus de property: **IS\_DICTIONARY**
	- Aquest property és molt similar al **IS\_ARRAY**, la diferència entre **IS\_ARRAY** i **IS\_DICTIONARY** és que aquest últim accepta també valors **stdClass** i els converteix automàticament a array (a:1, b:2…), en canvi el **IS\_ARRAY** accepta només arrays "plans" (a,b,c…).
- Moltes millores i optimitzacions.
- Solucionat bug amb el "trunc" dels decimals.
- Implementat el suport al nou driver del mongo (compatible amb php 7.x).
- Solucionat problema amb els mètodes has i els arrays (php 7.2 o superior).
- Canvis interns per adaptar els canvis al nou mongo driver.
	- Principalment ara ja no retorna arrays sinó stdClass.
- Millorada la encriptació dels properties **IS\_STRING\_ENCRYPTED**, ara fan ús de la llibreria **OpenSSL** enlloc del **mcrypt**.
	- S'ha millorat la forma d'encriptar, ja que ara el **iv** és random.


1.4.4
-----
- Solucionat bug al importar un property Object o Array quan el valor era **null**.

1.4.3
-----
- Implementada la funcionalitat d'obtenir el count les propietats del tipus Array.
	-  Els properties **IS\_ARRAY** ara tenen un mètode associat propertyName**Count**() igual que ho tenen els Booleans amb el seu **is**PropertyName() o els Date amb el seu propertyName**Str**()

1.4.2
-----
- Solucionat bug al importar un property DateTime quan el valor era **null**.

1.4.1
-----
- Solucionat bug al importar un property amb valor **null** (s'ignoraven els nulls i quedava el valor original assignal).

1.4.0
-----
- **Important:** Al importar informació des d'un **JSON** o **RawData** si el paràmetre **$writableOnly** és **true**, llavors es fan servir els setters enlloc d'assignar directament l'ivar. En el cas dels properties **IS\_STRING\_ENCRYPTED**, s'espera que la informació que arriba **NO** està encriptada.

- Afegida una nova classe ready-for-use anomenada **ExplicitEntity** que ve pre-configurada amb el property **$serializeClassName** a **true**.

- Afegida una nova classe ready-for-use anomenada **PlainEntity** que tècnicament és igual que la classe **Entity** però que **no fa ús dels MongoId** en els seus IDs.

1.3.2
-----
- Solucionat bug al serialitzar els properties que el seu valor era **0**, es consideraven "empty" i no s'afegien.

1.3.1
-----
- El DateFormatter ara retorna una excepció **DateFormatterInvalidStyle** enlloc d'un **Exception**.

1.3.0
-----
- Afegida la funcionalitat de poder auto-serialitzar i auto-deserialitzar els objectes. Aquesta funcionalitat és configurable a nivell de classe, és a dir, per defecte vé desactivat, si es vol fer ús d'aquesta funcionalitat s'han de definir els següents properties:
	- **$serializeClassName**: TRUE = Al serialitzar un objecte afageix una clau _class amb el nom de la classe.
	- **$serializeFullClassName** (opcional): TRUE = En el moment d'afegir la clau _class amb el nom de la classe també hi inclou el namespace (opció per defecte), en el cas de ser FALSE només hi posarà el nom de la classe. **ATENCIÓ**: Si no hi ha el namespace especificat, s'assumeix que les dos classes estan dins al mateix namespace!!
- Afegit mètode **pop\_ivar** per eliminar un element d'un array (i retornar-ne el seu valor). La característica d'aquest mètode és que deixa l'array amb els index re-ordenats, apart de verificar que el procés és correcte.

- Afegit mètode **writableProperties** per obtenir un llistat amb els noms dels properties que són modificables (READ\_WRITE o WRITE\_ONLY).

- Afegida la possibilitat limitar l'assignació dels properties al crear un objecte a partir d'un JSON o RawData, on només els properties modificables (marcats com READ\_WRITE i WRITE\_ONLY) s'assignin. Aquesta millora és útil per protegir els objectes a la hora de modificar-los des de una API, per exemple. Els mètodes afectats són:
	- public static function **instanceWithJson**(array $json, $replace = true, **$writableOnly = false**)
	- public function **fromJson**(array $json, $replace = true, **$writableOnly = false**)
	- public static function **instanceWithRawData**(array $rawData, $replace = true, **$writableOnly = false**)
	- public function **fromRawData**(array $rawData, $replace = true, **$writableOnly = false**)
	- protected function **importProperties**(array $data, $replace = true, $rawDataMode = false, **$writableOnly = false**)

- Afegit mètode **move\_ivar** per moure un element d'un array d'una posició a una altra.

- Millorat el property **push_ivar**, ara valida que la informació que se li posa és coherent amb la definició del property.

- En els properties del tipus **IS\_OBJECT** ja no és possible concatenar el **|null**, si es vol que es pugui assignar un **null** s'ha d'utilitzar el tipus **IS\_OBJECT\_NULLABLE**.
 
 
1.2.1
-----

- Solucionat problema amb el mètode **__className()** quan la classe no estava dins un namespace retornava **false** enlloc del nom de la classe.

1.2.0
-----

- Canviat el nom del mètode **isWriteable** a **isWritable** de la classe Objectum.
- Canviada la signatura dels mètodes de la classe **Serializable**, ara només accepten que la informació sigui un array i no stdClass, ja que el zephir no sap iterar els stdClass:
	- importProperties(**array** $data, $replace = true, $rawDataMode = false)
	- instanceWithJson(**array** $json, $replace = true)
	- fromJson(**array** $json, $replace = true)
	- instanceWithRawData(**array** $rawData, $replace = true)
	- fromRawData(**array** $rawData, $replace = true)

- Afegit un paràmetre extra al mètode "rawData($array = false)" de la classe **Serializable**. Quan $array = true, el resultat és en format Array enlloc de stdClass.
- Afegida una nova classe **Helpers** dins al **Tools**:
	- stdClassToArray($value) -> array
	
1.1.2
-----
- Solucionat bug al assignar un null a un property "DateTime"

1.1.1
-----
- Solucionat bug al encriptar un string de 16 bytes de longitud.
- Solucionat bug al carregar del mongo un objecte Bin i convertir-lo a string.

1.1.0
-----
- Implementada la funcionalitat de convertir les propietats del tipus Date a string.
	-  Els properties **IS\_DATE** ara tenen un mètode associat propertyName**Str**() igual que ho tenen els booleans amb el seu **is**PropertyName()

- Afegit un nou tipus de property **IS\_STRING\_ENCRYPTED** per guardar text encriptat. 
	- Aquests properties es guarden encriptats en memòria i només es desencripten quan es necessita.
		- **JSON**: Retorna el text encriptat en format base64.
		- **RawData**: Retorna un **MongoBinData** genèric.

- Solucionat un problema al serialitzar els JSON quan hi havia un property "write-only" (es generava una excepció de "només escriptura", ara simplement s'ignoren els properties que son de només escriptura).

1.0.x
-----
Primeres versions de la llibrería.
