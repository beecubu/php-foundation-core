<?php

use Beecubu\Foundation\Core\Tools\Formatters\DateFormatter;
use PHPUnit\Framework\TestCase;

class DateFormatterTests extends TestCase
{
    public function testWithServerTimeZone(): void
    {
        $date = new DateTime('01/01/2000 12:00:00');
        $string = DateFormatter::dateToString($date, 'es', DateFormatter::DATE_TIME_STYLE);
        $this->assertEquals('01/01/2000 12:00:00', $string);
    }

    public function testWithCustomTimeZone(): void
    {
        // set-up the TimeZone
        DateFormatter::$TimeZone = 'America/Mexico_City';
        // test TimeZone
        $date = new DateTime('01/01/2000 12:00:00');
        $string = DateFormatter::dateToString($date, 'es', DateFormatter::DATE_TIME_STYLE);
        $this->assertEquals('01/01/2000 05:00:00', $string);
    }
}