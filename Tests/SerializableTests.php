<?php

namespace Beecubu\Tests\Foundation\Core;

use Beecubu\Foundation\Core\CustomProperty;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;
use Beecubu\Foundation\Core\SerializableProperty;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Objecte amb diferents properties per provar el Serializable.
 *
 * @property string $encrypted_read_only
 * @property string $encrypted_read_write
 * @property string $encrypted_write_only
 *
 * @property string $binary
 *
 * @property mixed $mixed
 */
class SerializableObject extends Serializable
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'encrypted_read_only'  => [Property::READ_ONLY, Property::IS_STRING_ENCRYPTED],
            'encrypted_read_write' => [Property::READ_WRITE, Property::IS_STRING_ENCRYPTED],
            'encrypted_write_only' => [Property::WRITE_ONLY, Property::IS_STRING_ENCRYPTED],

            'mixed' => [Property::READ_WRITE, Property::IS_MIXED],

            'binary' => [Property::READ_WRITE, Property::IS_BINARY],
        ];
    }

    /**
     * Assigna un valor a un property, independentment de si és read-only o no.
     *
     * @param string $property El property a assignar.
     * @param mixed $value El valor a assignar.
     */
    public function assignIvar(string $property, $value): void
    {
        $this->set_ivar($property, $value);
    }

    /**
     * Retorna el valor d'un property (tal qual està dins l'ivar), independentment del tipus d'accés definit al property.
     *
     * @param string $property El property a obtenir.
     *
     * @return mixed El valor del property.
     */
    public function getIvar(string $property)
    {
        return $this->ivars[$property];
    }
}

/**
 * Objecte per provar el custom import/export dels properties
 */
class CustomExportImportProperties extends Serializable
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'prop1' => [Property::READ_WRITE, Property::IS_STRING],
            'prop2' => [Property::READ_WRITE, Property::IS_STRING],
            'prop3' => [Property::READ_WRITE, Property::IS_STRING],
            'prop4' => [Property::READ_WRITE, Property::IS_STRING],
            'prop5' => [Property::READ_WRITE, Property::IS_STRING],
            'prop6' => [Property::READ_WRITE, Property::IS_STRING],
        ];
    }

    protected function customProperties(): void
    {
        $this->customProperties += [
            'prop1' => CustomProperty::ON_IMPORT_AND_EXPORT_IN_ALL_MODES,
            'prop2' => CustomProperty::ON_IMPORT | CustomProperty::ON_JSON_MODE,
            'prop3' => CustomProperty::ON_EXPORT | CustomProperty::ON_JSON_MODE,
            'prop4' => CustomProperty::ON_IMPORT | CustomProperty::ON_RAW_MODE,
            'prop5' => CustomProperty::ON_EXPORT | CustomProperty::ON_RAW_MODE,
        ];
    }

    protected function exportCustomProperty(string $property, $value, bool $rawDataMode, ?array $fields = null, bool $excluding = true, bool $ignoreEmptyValues = true)
    {
        return $value.' - EXPORT';
    }

    protected function importCustomProperty(string $property, $value, bool $replace = true, bool $rawDataMode = false, bool $writableOnly = false)
    {
        return $value.' - IMPORT';
    }
}

/**
 * Objecte per provar els properties CREATE_ON_DEMAND_BY_ID
 *
 * @property string $id.
 * @property string $value.
 */
class CreateOnDemandByIdObjectExample extends Serializable
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'value' => [Property::READ_WRITE, Property::IS_STRING],
        ];
    }

    public function __construct()
    {
        parent::__construct();
        $this->set_ivar('id', '64ca4d91dc68c82f35378692');
        $this->set_ivar('value', 'testing');
    }

    protected static function instanceByRawDataId($id)
    {
        $obj = new static();
        if ($id !== '64ca4d91dc68c82f35378692')
        {
            $obj->set_ivar('id', $id);
            $obj->set_ivar('value', 'testing IS NOT 64ca4d91dc68c82f35378692');
        }
        return $obj;
    }


    protected static function parseDataBeforeInsertInIvarsOnDemand($data)
    {
        if (is_string($data))
        {
            return $data;
        }
        elseif (is_array($data) && isset($data['id']))
        {
            return $data['id'];
        }
        // uh...
        return parent::parseDataBeforeInsertInIvarsOnDemand($data);
    }
}

/**
 * Objecte per provar els properties CREATE_ON_DEMAND_BY_ID
 *
 * @property CreateOnDemandByIdObjectExample $object
 * @property CreateOnDemandByIdObjectExample[] $objects
 */
class CreateOnDemandByIdObject extends Serializable
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'object'  => [Property::READ_WRITE, Property::IS_OBJECT_NULLABLE, CreateOnDemandByIdObjectExample::class, SerializableProperty::CREATE_ON_DEMAND_BY_ID],
            'objects' => [Property::READ_WRITE, Property::IS_ARRAY, CreateOnDemandByIdObjectExample::class, SerializableProperty::CREATE_ON_DEMAND_BY_ID],
        ];
    }
}

/**
 * Objecte per provar el filterJson
 *
 * @property string $childA
 * @property string $childB
 */
class FilterJsonObj2 extends Serializable
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'childA' => [Property::READ_WRITE, Property::IS_STRING],
            'childB' => [Property::READ_WRITE, Property::IS_STRING],
        ];
    }
}

/**
 * Objecte per provar el filterJson
 *
 * @property string $propA
 * @property string $propB
 * @property FilterJsonObj2 $propC
 * @property FilterJsonObj2[] $propD
 */
class FilterJsonObj1 extends Serializable
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'propA' => [Property::READ_WRITE, Property::IS_STRING],
            'propB' => [Property::READ_WRITE, Property::IS_STRING],
            'propC' => [Property::READ_WRITE, Property::IS_OBJECT, FilterJsonObj2::class],
            'propD' => [Property::READ_WRITE, Property::IS_ARRAY, FilterJsonObj2::class],
        ];
    }
}

class SerializableTests extends TestCase
{
    public function testNullEncryptedStringPropertyWithWriteOnlyDisabled(): void
    {
        $obj1 = new SerializableObject();
        $obj1->encrypted_read_write = null;
        $raw = $obj1->rawData();
        $this->assertEquals($obj1->encrypted_read_write, $raw->encrypted_read_write);

        $obj1 = new SerializableObject();
        $obj1->encrypted_read_write = '123';
        $json = $obj1->json();
        $this->assertEquals(base64_encode($obj1->getIvar('encrypted_read_write')), $json->encrypted_read_write);
    }

    public function testJSONEncryptedStringPropertyWithWriteOnlyDisabled(): void
    {
        $obj1 = new SerializableObject();
        $obj1->assignIvar('encrypted_read_only', uniqid());
        $json = $obj1->json();
        $obj2 = SerializableObject::instanceWithJson($json);
        $this->assertEquals($obj1->encrypted_read_only, $obj2->encrypted_read_only);

        $obj1 = new SerializableObject();
        $obj1->encrypted_read_write = uniqid();
        $json = $obj1->json();
        $obj2 = SerializableObject::instanceWithJson($json);
        $this->assertEquals($obj1->encrypted_read_write, $obj2->encrypted_read_write);

        $obj1 = new SerializableObject();
        $obj1->encrypted_write_only = uniqid();
        $json = $obj1->json();
        $this->assertObjectNotHasAttribute('encrypted_write_only', $json);
    }

    public function testJSONEncryptedStringPropertyWithWriteOnlyEnabled(): void
    {
        $obj1 = new SerializableObject();
        $obj1->assignIvar('encrypted_read_only', uniqid());
        $json = $obj1->json();
        $obj2 = SerializableObject::instanceWithJson($json, true, true);
        $this->assertNull($obj2->encrypted_read_only);

        $obj1 = new SerializableObject();
        $obj1->encrypted_read_write = uniqid();
        $json = $obj1->json();
        $obj2 = SerializableObject::instanceWithJson($json, true, true);
        $this->assertEquals($obj1->encrypted_read_write, $obj2->encrypted_read_write);
    }

    public function testRAWDATAEncryptedStringPropertyWithWriteOnlyDisabled(): void
    {
        $obj1 = new SerializableObject();
        $obj1->assignIvar('encrypted_read_only', uniqid());
        $rawData = $obj1->rawData();
        $obj2 = SerializableObject::instanceWithRawData($rawData);
        $this->assertEquals($obj1->encrypted_read_only, $obj2->encrypted_read_only);

        $obj1 = new SerializableObject();
        $obj1->encrypted_read_write = uniqid();
        $rawData = $obj1->rawData();
        $obj2 = SerializableObject::instanceWithRawData($rawData);
        $this->assertEquals($obj1->encrypted_read_write, $obj2->encrypted_read_write);

        $obj1 = new SerializableObject();
        $obj1->encrypted_write_only = uniqid();
        $rawData = $obj1->rawData();
        $obj2 = SerializableObject::instanceWithRawData($rawData);
        $this->assertEquals($obj1->getIvar('encrypted_write_only'), $obj2->getIvar('encrypted_write_only'));
    }

    public function testRAWDATAEncryptedStringPropertyWithWriteOnlyEnabled(): void
    {
        $obj1 = new SerializableObject();
        $obj1->assignIvar('encrypted_read_only', uniqid());
        $rawData = $obj1->rawData();
        $obj2 = SerializableObject::instanceWithRawData($rawData, true, true);
        $this->assertNull($obj2->encrypted_read_only);

        $obj1 = new SerializableObject();
        $obj1->encrypted_read_write = uniqid();
        $rawData = $obj1->rawData();
        $obj2 = SerializableObject::instanceWithRawData($rawData, true, true);
        $this->assertEquals($obj1->encrypted_read_write, $obj2->encrypted_read_write);

        $obj1 = new SerializableObject();
        $obj1->encrypted_write_only = uniqid();
        $rawData = $obj1->rawData();
        $obj2 = SerializableObject::instanceWithRawData($rawData, true, true);
        $this->assertEquals($obj1->getIvar('encrypted_write_only'), $obj2->getIvar('encrypted_write_only'));
    }

    public function testCustomImportProperties(): void
    {
        $data = ['prop1' => 'a', 'prop2' => 'b', 'prop3' => 'c', 'prop4' => 'd', 'prop5' => 'e', 'prop6' => 'f'];

        $obj1 = CustomExportImportProperties::instanceWithJson($data);
        $this->assertEquals('a - IMPORT', $obj1->prop1);
        $this->assertEquals('b - IMPORT', $obj1->prop2);
        $this->assertEquals('c', $obj1->prop3);
        $this->assertEquals('d', $obj1->prop4);
        $this->assertEquals('e', $obj1->prop5);
        $this->assertEquals('f', $obj1->prop6);

        $obj1 = CustomExportImportProperties::instanceWithRawData($data);
        $this->assertEquals('a - IMPORT', $obj1->prop1);
        $this->assertEquals('b', $obj1->prop2);
        $this->assertEquals('c', $obj1->prop3);
        $this->assertEquals('d - IMPORT', $obj1->prop4);
        $this->assertEquals('e', $obj1->prop5);
        $this->assertEquals('f', $obj1->prop6);
    }

    public function testCustomExportProperties(): void
    {
        $obj = new CustomExportImportProperties();
        $obj->prop1 = 'a';
        $obj->prop2 = 'b';
        $obj->prop3 = 'c';
        $obj->prop4 = 'd';
        $obj->prop5 = 'e';
        $obj->prop6 = 'f';

        $data = $obj->json();
        $this->assertEquals('a - EXPORT', $data->prop1);
        $this->assertEquals('b', $data->prop2);
        $this->assertEquals('c - EXPORT', $data->prop3);
        $this->assertEquals('d', $data->prop4);
        $this->assertEquals('e', $data->prop5);
        $this->assertEquals('f', $data->prop6);

        $data = $obj->rawData();
        $this->assertEquals('a - EXPORT', $data->prop1);
        $this->assertEquals('b', $data->prop2);
        $this->assertEquals('c', $data->prop3);
        $this->assertEquals('d', $data->prop4);
        $this->assertEquals('e - EXPORT', $data->prop5);
        $this->assertEquals('f', $data->prop6);
    }

    public function testExportMixedValue(): void
    {
        $obj = new SerializableObject();
        $obj->mixed = 'test';
        $this->assertEquals($obj->mixed, $obj->json()->mixed);

        $obj = new SerializableObject();
        $obj->mixed = 1.2;
        $this->assertEquals($obj->mixed, $obj->json()->mixed);

        $obj = new SerializableObject();
        $obj->mixed = true;
        $this->assertEquals($obj->mixed, $obj->json()->mixed);

        $obj = new SerializableObject();
        $obj->mixed = ['a', 1, true];
        $this->assertEquals($obj->mixed, $obj->json()->mixed);

        $obj = new SerializableObject();
        $obj->mixed = new SerializableObject();
        $obj->mixed->mixed = 'test';
        $json = $obj->json();
        $this->assertInstanceOf(stdClass::class, $json->mixed);
        $this->assertEquals($obj->mixed->mixed, $json->mixed->mixed);

        $obj1 = new SerializableObject();
        $obj1->mixed = 'test';
        $obj2 = new SerializableObject();
        $obj2->mixed = [$obj1, true];
        $json = $obj2->json();
        $this->assertIsArray($obj2->mixed);
        $this->assertInstanceOf(stdClass::class, $json->mixed[0]);
        $this->assertEquals($obj2->mixed[1], $json->mixed[1]);
    }

    public function testCreateOnDemandAssignNullValue(): void
    {
        $obj = new CreateOnDemandByIdObject();
        $obj->object = new CreateOnDemandByIdObjectExample();
        $raw = $obj->rawData();
        $this->assertEquals('64ca4d91dc68c82f35378692', $raw->object);

        $json = ['object' => ['id' => '64ca525e796d5226fd5e7882', 'value' => 'testing2']];
        $obj = new CreateOnDemandByIdObject();
        $obj->fromJson($json, false, true);
        $this->assertInstanceOf(CreateOnDemandByIdObjectExample::class, $obj->object);
        $this->assertEquals('64ca525e796d5226fd5e7882', $obj->object->id);

        $json = [
            'objects' => [
                ['id' => '64ca525e796d5226fd5e2222', 'value' => 'testing2'],
                ['id' => '64ca525e796d5226fd5e3333', 'value' => 'testing3'],
            ],
        ];
        $obj = new CreateOnDemandByIdObject();
        $obj->fromJson($json, false, true);
        $this->assertIsArray($obj->objects);
        $this->assertEquals('64ca525e796d5226fd5e2222', $obj->objects[0]->id);
        $obj->objects = null;
        $this->assertEmpty($obj->objects);

        $json = [
            'object' => ['id' => '64ca525e796d5226fd5e2222', 'value' => 'testing2'],
        ];
        $obj = new CreateOnDemandByIdObject();
        $obj->fromJson($json, false, true);
        $json = ['object' => null];
        $obj->fromJson($json, false, true);
        $this->assertNull($obj->object);
    }

    public function testJsonExcluded(): void
    {
        $obj1 = new FilterJsonObj1();
        $obj1->propA = 'A';
        $obj1->propB = 'B';
        $obj1->propC = new FilterJsonObj2();
        $obj1->propC->childA = 'A';
        $obj1->propC->childB = 'B';
        $arr = [];
        $obj2 = new FilterJsonObj2();
        $obj2->childA = '1.A';
        $obj2->childB = '1.B';
        $arr[] = $obj2;
        $obj2 = new FilterJsonObj2();
        $obj2->childA = '2.A';
        $obj2->childB = '2.B';
        $arr[] = $obj2;
        $obj1->propD = $arr;

        $fields = ['propA', 'propC' => ['childA'], 'propD' => ['childB']];
        $filtered = $obj1->json($fields, true);

        $this->assertIsObject($filtered);
        $this->assertObjectNotHasAttribute('propA', $filtered);
        $this->assertObjectHasAttribute('propB', $filtered);
        $this->assertObjectHasAttribute('propC', $filtered);
        $this->assertObjectNotHasAttribute('childA', $filtered->propC);
        $this->assertObjectHasAttribute('childB', $filtered->propC);
        $this->assertObjectHasAttribute('propD', $filtered);
        $this->assertIsArray($filtered->propD);
        $this->assertCount(2, $filtered->propD);
        $this->assertObjectHasAttribute('childA', $filtered->propD[0]);
        $this->assertObjectNotHasAttribute('childB', $filtered->propD[0]);
        $this->assertObjectHasAttribute('childA', $filtered->propD[1]);
        $this->assertObjectNotHasAttribute('childB', $filtered->propD[1]);
    }

    public function testJsonFiltered(): void
    {
        $obj1 = new FilterJsonObj1();
        $obj1->propA = 'A';
        $obj1->propB = 'B';
        $obj1->propC = new FilterJsonObj2();
        $obj1->propC->childA = 'A';
        $obj1->propC->childB = 'B';
        $arr = [];
        $obj2 = new FilterJsonObj2();
        $obj2->childA = '1.A';
        $obj2->childB = '1.B';
        $arr[] = $obj2;
        $obj2 = new FilterJsonObj2();
        $obj2->childA = '2.A';
        $obj2->childB = '2.B';
        $arr[] = $obj2;
        $obj1->propD = $arr;

        $fields = ['propA', 'propC' => ['childA'], 'propD' => ['childB']];
        $filtered = $obj1->json($fields, false);

        $this->assertIsObject($filtered);
        $this->assertObjectHasAttribute('propA', $filtered);
        $this->assertObjectNotHasAttribute('propB', $filtered);
        $this->assertObjectHasAttribute('propC', $filtered);
        $this->assertObjectHasAttribute('childA', $filtered->propC);
        $this->assertObjectNotHasAttribute('childB', $filtered->propC);
        $this->assertObjectHasAttribute('propD', $filtered);
        $this->assertIsArray($filtered->propD);
        $this->assertCount(2, $filtered->propD);
        $this->assertObjectNotHasAttribute('childA', $filtered->propD[0]);
        $this->assertObjectHasAttribute('childB', $filtered->propD[0]);
        $this->assertObjectNotHasAttribute('childA', $filtered->propD[1]);
        $this->assertObjectHasAttribute('childB', $filtered->propD[1]);
    }

    public function testBinaryProperty(): void
    {
        $obj1 = new SerializableObject();
        $obj1->binary = '12345';
        $this->assertEquals(base64_encode($obj1->binary), $obj1->rawData());
    }
}
