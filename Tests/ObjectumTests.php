<?php

namespace Beecubu\Tests\Foundation\Core;

use Beecubu\Foundation\Core\Enum;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidArrayEnumElementsException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyEnumValueException;
use Beecubu\Foundation\Core\Objectum;
use Beecubu\Foundation\Core\Property;
use DateTime;
use PHPUnit\Framework\TestCase;

/**
 * Enum d'exemple.
 */
class EnumA extends Enum
{
    public const A = 'a';
    public const B = 'b';
    public const C = 'c';
}

/**
 * @property string $propA
 */
class ObjectumDummy extends Objectum
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'propA' => [Property::READ_WRITE, Property::IS_STRING],
        ];
    }
}

/**
 * Objecte amb diferents properties per provar els Enums.
 *
 * @property string $enum
 * @property string $enum_nullable
 * @property string[] $enum_array
 *
 * @property mixed $object_eval
 *
 * @property ObjectumDummy $dummyA
 * @property ObjectumDummy $dummyB
 * @property ObjectumDummy $dummyC
 *
 * @method bool hasDummyA()
 * @method bool hasDummyB()
 * @method bool hasDummyC()
 */
class ObjectumTest extends Objectum
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'enum'          => [Property::READ_WRITE, Property::IS_ENUM, EnumA::class],
            'enum_nullable' => [Property::READ_WRITE, Property::IS_ENUM_NULLABLE, EnumA::class],
            'enum_array'    => [Property::READ_WRITE, Property::IS_ARRAY_ENUM, EnumA::class],

            'object_eval' => [Property::READ_WRITE, Property::IS_MIXED],

            'dummyA' => [Property::READ_WRITE, Property::IS_OBJECT, ObjectumDummy::class],
            'dummyB' => [Property::READ_WRITE, Property::IS_OBJECT_NULLABLE, ObjectumDummy::class],
            'dummyC' => [Property::READ_WRITE, Property::IS_OBJECT, ObjectumDummy::class],
        ];
    }

    protected function getDummyC()
    {
        if ( ! ($this->ivars['dummyC'] ?? null))
        {
            $dummy = new ObjectumDummy();
            $dummy->propA = '14';

            $this->set_ivar('dummyC', $dummy);
        }
        return $this->ivars['dummyC'];
    }
}

/**
 * Objecte que hereda de l'objecte de proves.
 *
 * @property mixed $extra_property
 */
class ObjectumTestExtended extends ObjectumTest
{
    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'extra_property' => [Property::READ_WRITE, Property::IS_MIXED],
        ];
    }
}

class PlainObject
{
    public $object = null;
}

class ObjectumTests extends TestCase
{
    public function testValidEnums(): void
    {
        $obj = new ObjectumTest();

        $obj->enum = EnumA::A;
        self::assertEquals($obj->enum, EnumA::A);

        $obj->enum_nullable = null;
        self::assertEquals($obj->enum_nullable, null);

        $obj->enum_array = [EnumA::A, EnumA::B, EnumA::C];
        self::assertEquals($obj->enum_array, [EnumA::A, EnumA::B, EnumA::C]);
    }

    public function testAssignInvalidValueOnEnumProperty(): void
    {
        self::expectException(ObjectInvalidPropertyEnumValueException::class);

        $obj = new ObjectumTest();
        $obj->enum = 'E';
    }

    public function testAssignNullOnEnumPropertyWhenIsNotNullable(): void
    {
        self::expectException(ObjectInvalidPropertyEnumValueException::class);

        $obj = new ObjectumTest();
        $obj->enum = null;
    }

    public function testAssignInvalidValueOnArrayEnumProperty(): void
    {
        self::expectException(ObjectInvalidArrayEnumElementsException::class);

        $obj = new ObjectumTest();
        $obj->enum_array = ['E'];
    }

    public function testMagicIssetMethodOnAssignedProperty(): void
    {
        $obj = new ObjectumTest();
        $obj->enum = EnumA::A;

        $this->assertFalse(empty($obj->enum));
        $this->assertTrue(isset($obj->enum));
    }

    public function testMagicIssetMethodOnUnassignedProperty(): void
    {
        $obj = new ObjectumTest();

        $this->assertTrue(empty($obj->enum));
        $this->assertFalse(isset($obj->enum));
    }

    public function testMagicIssetMethodOnArrayProperty(): void
    {
        $obj = new ObjectumTest();
        $obj->enum_array = [EnumA::A, EnumA::B, EnumA::C];

        $this->assertFalse(empty($obj->enum_array));
        $this->assertTrue(isset($obj->enum_array));
    }

    public function testMagicIssetMethodOnEmptyArrayProperty(): void
    {
        $obj = new ObjectumTest();

        $this->assertTrue(empty($obj->enum_array));
        $this->assertTrue(isset($obj->enum_array));
    }

    public function testGetNestedPropertyValueWithFinalPHPObject(): void
    {
        $obj = new ObjectumTest();
        $obj->object_eval = new DateTime();

        $this->assertEquals($obj->getNestedPropertyValue('object_eval.format("d/m/Y")'), (new DateTime())->format('d/m/Y'));
    }

    public function testGetNestedPropertyValueWithPHPObjectWithObjectum(): void
    {
        $obj = new ObjectumTest();
        $obj->object_eval = new PlainObject();
        $obj->object_eval->object = new ObjectumTest();
        $obj->object_eval->object->object_eval = new DateTime();

        $this->assertEquals($obj->getNestedPropertyValue('object_eval.object.object_eval.format("d/m/Y")'), (new DateTime())->format('d/m/Y'));
    }

    public function testCloneObjectToSameClass(): void
    {
        $obj1 = new ObjectumTest();
        $obj1->object_eval = 'hi';

        $obj2 = $obj1->cloneAsClass(get_class($obj1));

        $this->assertInstanceOf(ObjectumTestExtended::class, get_class($obj2));
    }

    public function testCloneObjectToAnotherClass(): void
    {
        $obj1 = new ObjectumTest();
        $obj1->object_eval = 'hi';

        $obj2 = $obj1->cloneAsClass(ObjectumTestExtended::class);

        $this->assertInstanceOf(ObjectumTestExtended::class, get_class($obj2));
    }

    public function testCloneObjectToAnotherWithLessProperties(): void
    {
        $obj1 = new ObjectumTestExtended();
        $obj1->object_eval = 'hi';
        $obj1->extra_property = 'bye';

        $obj2 = $obj1->cloneAsClass(ObjectumTest::class);

        $this->assertInstanceOf(ObjectumTest::class, get_class($obj2));
    }

    public function testGetObjectNotInitilizedProperty(): void
    {
        $obj1 = new ObjectumTest();
        $obj1->dummyA->propA = '12';

        $this->assertNotNull($obj1->dummyA);
        $this->assertEquals('12', $obj1->dummyA->propA);

        $obj2 = new ObjectumTest();
        $obj2->dummyA = new ObjectumDummy();
        $obj2->dummyA->propA = '13';

        $this->assertNotNull($obj2->dummyA);
        $this->assertEquals('13', $obj2->dummyA->propA);

        $obj3 = new ObjectumTest();
        $this->assertNotNull($obj3->dummyA);

        $obj4 = new ObjectumTest();
        $this->assertTrue($obj4->hasDummyA());
        $this->assertFalse($obj4->hasDummyB());

        $obj5 = new ObjectumTest();
        $this->assertTrue($obj5->hasDummyC());
        $this->assertEquals($obj5->dummyC->propA, '14');
    }
}
