<?php

namespace Beecubu\Foundation\Core;

abstract class Property
{
    // kind of properties
    public const READ_ONLY  = 'read-only';
    public const READ_WRITE = 'read-write';
    public const WRITE_ONLY = 'write-only';

    // Type of properties
    public const IS_ARRAY            = 'array';
    public const IS_ARRAY_ENUM       = 'array_enum';
    public const IS_BOOLEAN          = 'boolean';
    public const IS_DATE             = 'date';
    public const IS_DICTIONARY       = 'dictionary';
    public const IS_DOUBLE           = 'double';
    public const IS_ENUM             = 'enum';
    public const IS_ENUM_NULLABLE    = 'enum_nullable';
    public const IS_FLOAT            = 'float';
    public const IS_INTEGER          = 'integer';
    public const IS_MIXED            = 'mixed';
    public const IS_OBJECT           = 'object';
    public const IS_OBJECT_NULLABLE  = 'object_nullable';
    public const IS_PROPERTY_ALIAS   = 'property_alias';
    public const IS_STRING           = 'string';
    public const IS_STRING_ENCRYPTED = 'string_encrypted';
    public const IS_BINARY           = 'binary';
}
