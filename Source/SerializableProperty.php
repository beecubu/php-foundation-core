<?php

namespace Beecubu\Foundation\Core;

abstract class SerializableProperty
{
    const CREATE_ON_SERIALIZE_BY_ID = 'create-on-serialize-by-id';
    const CREATE_ON_DEMAND_BY_ID    = 'create-on-demand-by-id';
}