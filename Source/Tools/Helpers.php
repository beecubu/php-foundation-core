<?php

namespace Beecubu\Foundation\Core\Tools;

use stdClass;

/**
 * Conjunt d'utilitats per ajudar a tasques comuns.
 */
class Helpers
{
    /**
     * Converteix un stdClass a array
     *
     * @param mixed $value El valor a convertir.
     *
     * @return array L'objecte stdClass convertit a array.
     */
    public static function stdClassToArray($value)
    {
        if (is_object($value) && $value instanceof stdClass)
        {
            $value = get_object_vars($value);
        }
        // parse the array
        if (is_array($value))
        {
            return array_map(__CLASS__.'::'.__FUNCTION__, $value);
        }
        else // return as is...
        {
            return $value;
        }
    }
}
