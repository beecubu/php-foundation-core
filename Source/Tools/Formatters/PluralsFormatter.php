<?php

namespace Beecubu\Foundation\Core\Tools\Formatters;

/**
 * Converteix a plural certes paraules com dia, setmana, mes, any...
 */
class PluralsFormatter
{
    public static function plural($word, $count, $locale)
    {
        $words = [
            'es' => [
                'second' => ['segundo', 'segundos'],
                'minute' => ['minuto', 'minutos'],
                'hour'   => ['hora', 'horas'],
                'day'    => ['dia', 'dias'],
                'week'   => ['semana', 'semanas'],
                'month'  => ['mes', 'meses'],
                'year'   => ['año', 'años'],
            ],
            'en' => [
                'second' => ['second', 'seconds'],
                'minute' => ['minute', 'minutes'],
                'hour'   => ['hour', 'hours'],
                'day'    => ['day', 'days'],
                'week'   => ['week', 'weeks'],
                'month'  => ['month', 'months'],
                'year'   => ['year', 'years'],
            ],
        ];
        // return the plural
        return $count === 1 ? $words[$locale][$word][0] : $words[$locale][$word][1];
    }
}