<?php

namespace Beecubu\Foundation\Core\Tools\Formatters;

/**
 * Formateja els números.
 */
class NumberFormatter
{
    /**
     * Converteix un numero a text amb un numero concret de decimals (2 per defecte)
     *
     * @param float $value El numero a convertir
     * @param string $locale Id de l'idioma amb el que formatejar.
     * @param integer $decimals Número de decimals (2 per defecte).
     * @param boolean $trunc TRUE = El numero es trunca a 2 decimals (per defecte), FALSE = arrodoneix a 2 decimals
     *
     * @return string El numero convertit a text.
     */
    public static function numberToString($value, $locale, $decimals = 2, $trunc = true)
    {
        $numberFormat = [
            'es' => [',', '.'],
            'en' => ['.', ','],
        ];
        // convert to lowe
        $locale = strtolower($locale);
        // should trunk decimals?
        if ($trunc && ($dec = strstr($value, '.')) !== false)
        {
            $value = floor($value) + (float)substr($dec, 0, $decimals + 1);
        }
        //convert to string
        return number_format($value, $decimals, $numberFormat[$locale][0], $numberFormat[$locale][1]);
    }

    /**
     * Converteix un numero a text amb
     *
     * @param float $value La quantitat que es vol formatejar.
     * @param string $locale Id de l'idioma amb el que formatejar.
     * @param string $currency Id de la moneda a utilitzar.
     *
     * @return string El numero convertit a text.
     */
    public static function currencyToString($value, $locale, $currency)
    {
        $currencies = [
            'es' => ['eur' => '%s€', 'usd' => '%s$', 'gbp' => '%s£'],
            'en' => ['eur' => '€%s', 'usd' => '$%s', 'gbp' => '£%s'],
        ];
        // convert it
        return sprintf($currencies[$locale][strtolower($currency)], NumberFormatter::numberToString($value, $locale));
    }

    /**
     * Converteix un numero a text amb dos decimals i sense comes (les dues últimes xifres són els decimals)
     *
     * @param float $value El numero a convertir
     *
     * @return String El numero convertit a text.
     */
    public static function currencyToStringTwoDecimalsNoComma($value)
    {
        // convert to string
        $string = NumberFormatter::numberToString($value, 'es', 2);
        // strip commas and dots
        $string = str_replace(',', '', $string);
        $string = str_replace('.', '', $string);
        // return text
        return $string;
    }
}
