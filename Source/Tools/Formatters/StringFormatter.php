<?php

namespace Beecubu\Foundation\Core\Tools\Formatters;

/**
 * Formateja els strings
 */
class StringFormatter
{
    /**
     * Converteix un string UTF8 a format ISO20022 (sense accents ni caràcters no ASCII bàsic)
     *
     * @param string $value El text a convertir
     *
     * @return String El text convertit a ISO20022
     */
    public static function utf82iso($value)
    {
        return strtr(utf8_decode($value), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }
}
