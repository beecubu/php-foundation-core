<?php

namespace Beecubu\Foundation\Core\Tools\Formatters;

use Beecubu\Foundation\Core\Exceptions\DateFormatterInvalidStyleException;
use DateTime;
use IntlDateFormatter;

/**
 * Formateja les dates segons l'idioma
 */
class DateFormatter
{
    // static TimeZone config property
    public static $TimeZone = null; // null = PHP configuration

    const DATE_TIME_STYLE          = 'date_time';
    const TIME_DATE_STYLE          = 'time_date';
    const ONLY_DATE_STYLE          = 'date';
    const ONLY_TIME_STYLE          = 'time';
    const RELATIVE_DATE_TIME_STYLE = 'relative_date_time';
    const RELATIVE_ONLY_DATE_STYLE = 'relative_date';
    const RELATIVE_ONLY_TIME_STYLE = 'relative_time';

    /**
     * Converteix una data a text segons l'idioma.
     *
     * @param DateTime $date La data a formatejar.
     * @param string $locale Id de l'idioma amb el que formatejar.
     * @param string $style Tipus de formatejat (DateTimeStyle, OnlyDateStyle o OnlyTimeStyle)
     *
     * @return String La data convertida a text.
     *
     * @throws DateFormatterInvalidStyleException
     */
    public static function dateToString(DateTime $date, $locale, $style = DateFormatter::DATE_TIME_STYLE)
    {
        if (is_null($date))
        {
            return '-';
        }
        // array with date formats
        $dateFormat = [
            'es' => ['HH:mm:ss', 'dd/MM/yyyy'],
            'en' => ['HH:mm:ss', 'MM/dd/yyyy'],
        ];
        // initialization
        $format = null;
        $locale = strtolower($locale);
        // determine the format
        switch ($style)
        {
            case DateFormatter::DATE_TIME_STYLE:
                $format = "{$dateFormat[$locale][1]} {$dateFormat[$locale][0]}";
                break;
            case DateFormatter::TIME_DATE_STYLE:
                $format = "{$dateFormat[$locale][0]} {$dateFormat[$locale][1]}";
                break;
            case DateFormatter::ONLY_DATE_STYLE:
                $format = $dateFormat[$locale][1];
                break;
            case DateFormatter::ONLY_TIME_STYLE:
                $format = $dateFormat[$locale][0];
                break;
            case DateFormatter::RELATIVE_DATE_TIME_STYLE:
            case DateFormatter::RELATIVE_ONLY_DATE_STYLE:
            case DateFormatter::RELATIVE_ONLY_TIME_STYLE:
                {
                    // array with string values
                    static $strings = [
                        'es' => [
                            'now'          => 'ahora',
                            'few seconds'  => 'hace pocos segundos',
                            'today'        => 'hoy',
                            'today at'     => 'hoy a las',
                            'yesterday'    => 'yesterday',
                            'yesterday at' => 'yesterday at',
                        ],
                        'en' => [
                            'now'          => 'now',
                            'few seconds'  => 'few seconds',
                            'today'        => 'today',
                            'today at'     => 'today at',
                            'yesterday'    => 'ayer',
                            'yesterday at' => 'ayer a las',
                        ],
                    ];
                    /*
                       // calculation sheet
                        00 - 05 sec -> ahora
                        05 - 10 sec -> hace pocos segundos
                        10 - 59 sec -> X segundos
                        01 - 59 min -> X minutos
                        01 - 06 h   -> X horas
                        > 06 - today     -> hoy a las X
                        > 06 - yesterday -> ayer a las X
                        > yesterday m    -> full date
                     */
                    // get dates diff
                    $diff = $date->diff(new DateTime());
                    // les than one day
                    if ($diff->days === 0)
                    {
                        if ($diff->h === 0)
                        {
                            if ($diff->i === 0)
                            {
                                // 00 - 05 sec -> ahora
                                if ($diff->s <= 5) return $strings[$locale]['now'];
                                if ($diff->s <= 10) return $strings[$locale]['few seconds'];
                                // 10 - 59 sec -> hace pocos segundos
                                return $diff->s.' '.PluralsFormatter::plural('second', $diff->s, $locale);
                            }
                            else // 01 - 59 min -> X minutos
                            {
                                return $diff->i.' '.PluralsFormatter::plural('minute', $diff->i, $locale);
                            }
                        }
                        elseif ($diff->h < 6)
                        {
                            return $diff->h.' '.PluralsFormatter::plural('hour', $diff->i, $locale);
                        }
                    }
                    // only time?
                    if ($style === DateFormatter::RELATIVE_ONLY_TIME_STYLE)
                    {
                        return DateFormatter::dateToString($date, $locale, DateFormatter::ONLY_TIME_STYLE);
                    }
                    // only date or both
                    if ($date >= new DateTime('today'))
                    {
                        if ($style === DateFormatter::RELATIVE_DATE_TIME_STYLE)
                        {
                            return $strings[$locale]['today at'].' '.DateFormatter::dateToString($date, $locale, DateFormatter::ONLY_TIME_STYLE);
                        }
                        else // RELATIVE_ONLY_DATE_STYLE
                        {
                            return $strings[$locale]['today'];
                        }
                    }
                    elseif ($date >= new DateTime('yesterday'))
                    {
                        if ($style === DateFormatter::RELATIVE_DATE_TIME_STYLE)
                        {
                            return $strings[$locale]['yesterday at'].' '.DateFormatter::dateToString($date, $locale, DateFormatter::ONLY_TIME_STYLE);
                        }
                        else // RELATIVE_ONLY_DATE_STYLE
                        {
                            return $strings[$locale]['yesterday'];
                        }
                    }
                    elseif ($style === DateFormatter::RELATIVE_DATE_TIME_STYLE)
                    {
                        return DateFormatter::dateToString($date, $locale, DateFormatter::DATE_TIME_STYLE);
                    }
                    // RELATIVE_ONLY_DATE_STYLE
                    return DateFormatter::dateToString($date, $locale, DateFormatter::ONLY_DATE_STYLE);
                }
                break;
            default:
                throw new DateFormatterInvalidStyleException($style);
        }
        // configure the Intl Formatter
        $dateTimeType = IntlDateFormatter::NONE;
        // convert date using the Intl Date Formatter
        return IntlDateFormatter::create(null, $dateTimeType, $dateTimeType, self::$TimeZone, null, $format)->format($date);
    }
}
