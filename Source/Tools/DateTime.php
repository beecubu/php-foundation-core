<?php

namespace Beecubu\Foundation\Core\Tools;

use DateTimeZone;

/**
 * Soluciona el problema del TimeZone quan es crea un DateTime amb un timestamp.
 */
class DateTime extends \DateTime
{
    static private $ini_timezone = null;

    public function __construct($time = 'now', DateTimeZone $timezone = null)
    {
        parent::__construct($time, $timezone);
        // reassign
        if (is_null($timezone))
        {
            if (is_null(self::$ini_timezone))
            {
                self::$ini_timezone = new DateTimeZone(ini_get('date.timezone'));
            }
            // assign the time zone used in php.ini
            $this->setTimezone(self::$ini_timezone);
        }
    }
}
