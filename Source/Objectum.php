<?php

namespace Beecubu\Foundation\Core;

use Beecubu\Foundation\Core\Exceptions\DateFormatterInvalidStyleException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidArrayEnumElementsException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyAliasException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyArrayInstanceException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyEnumInstanceException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyEnumValueException;
use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyInstanceException;
use Beecubu\Foundation\Core\Exceptions\ObjectMissingPropertyException;
use Beecubu\Foundation\Core\Exceptions\ObjectPropertyIsNotAnArrayException;
use Beecubu\Foundation\Core\Exceptions\ObjectPropertyIsNotAnEncryptedStringException;
use Beecubu\Foundation\Core\Exceptions\ObjectReadOnlyException;
use Beecubu\Foundation\Core\Exceptions\ObjectUnknownMethodException;
use Beecubu\Foundation\Core\Exceptions\ObjectWriteOnlyException;
use Beecubu\Foundation\Core\Tools\Formatters\DateFormatter;
use DateTime;
use stdClass;

/**
 * Objecte bàsic del Foundation.
 */
class Objectum
{
    public static $CreateObjectPropertiesOnGet = true;

    protected $string_encrypted_key = '';

    protected $properties = [];
    protected $ivars      = [];

    // Properties definition

    protected function properties()
    {
        // the properties should be declared in sub-classes of Object
    }

    // Properties synthesize

    /**
     * El mètode isset() de qualsevol dels objectes Object,
     *
     * @param string $name El nom del property que es vol comprovar.
     *
     * @return bool TRUE = Està assignat, FALSE = no.
     *
     * @throws ObjectMissingPropertyException
     */
    public function __isset(string $name)
    {
        if ($this->hasProperty($name))
        {
            $result = isset($this->ivars[$name]);
            // is not present in ivars? maybe is a "lazy-load" value?
            if ( ! $result) $result = $this->{$name} !== null;
            // return the response
            return $result;
        }
        else // property not defined
        {
            $this->throwMissingPropertyException($name);
        }
    }

    /**
     * El mètode Getter de qualsevol dels objectes Object
     *
     * @param string $name El nom del property que es vol obtenir.
     *
     * @return mixed El valor del property.
     *
     * @throws ObjectInvalidPropertyAliasException
     * @throws ObjectMissingPropertyException
     * @throws ObjectWriteOnlyException
     */
    public function __get(string $name)
    {
        // execute the automatic getter
        if ($this->hasProperty($name))
        {
            if ($this->properties[$name][0] === Property::WRITE_ONLY)
            {
                $this->throwWriteOnlyException($name);
            }
            else // read-write or write-only
            {
                $getter = 'get'.ucwords($name);
                // specific getter
                if (method_exists($this, $getter))
                {
                    return $this->$getter();
                }
                // is a property alias
                elseif ($this->properties[$name][1] === Property::IS_PROPERTY_ALIAS)
                {
                    if (isset($this->properties[$name][2]))
                    {
                        return $this->getNestedPropertyValue($this->properties[$name][2]);
                    }
                    // invalid property alias (the alias is missing)
                    $this->throwInvalidPropertyAliasException($name);
                }
                // is an encrypted string
                elseif ($this->properties[$name][1] === Property::IS_STRING_ENCRYPTED)
                {
                    return $this->get_encrypted_ivar($name);
                }
                // is an object (NOT nullable)
                elseif ($this->properties[$name][1] === Property::IS_OBJECT && $this->properties[$name][2] && ! isset($this->ivars[$name]))
                {
                    if (static::$CreateObjectPropertiesOnGet)
                    {
                        $class = $this->properties[$name][2];
                        // create the object
                        $this->set_ivar($name, new $class());
                    }
                    return $this->ivars[$name] ?? null;
                }
                // default getter (high priority)
                elseif (isset($this->ivars[$name]))
                {
                    return $this->ivars[$name];
                }
                // is an array or dictionary?
                elseif ($this->is_array_property($name) || $this->properties[$name][1] === Property::IS_DICTIONARY)
                {
                    return [];
                }
                else // no value assigned (null)
                {
                    return null;
                }
            }
        }
        else // property not defined
        {
            $this->throwMissingPropertyException($name);
        }
    }

    /**
     * El mètode Setter de qualsevol dels objectes Object
     *
     * @param string $name El property a modificar
     * @param mixed $value El valor que es vol assignar
     *
     * @throws ObjectMissingPropertyException
     * @throws ObjectReadOnlyException
     */
    public function __set(string $name, $value)
    {
        // execute the automatic setter
        if ($this->hasProperty($name))
        {
            if ($this->properties[$name][0] === Property::READ_ONLY)
            {
                $this->throwReadOnlyException($name);
            }
            else // read-write or write-only
            {
                $setter = 'set'.ucwords($name);
                // specific setter
                if (method_exists($this, $setter))
                {
                    $this->$setter($value);
                }
                else // default setter
                {
                    $this->set_ivar($name, $value);
                }
            }
        }
        else // property not defined
        {
            $this->throwMissingPropertyException($name);
        }
    }

    /**
     * Determina quin tipus de mètode màgic s'esta cridant.
     *
     * @param string $name El nom del property
     * @param array $matches El resultat del matching, útil per post-processar el magic method.
     *
     * @return string El tipus de magic method, en el cas de que no estro
     */
    protected function magicMethodSignatureEvaluator(string $name, ?array &$matches = null)
    {
        // is a "has" function?
        if (preg_match('/^has([A-Z]\w*)/', $name, $matches))
        {
            return 'has';
        }
        // is a "is" function? Only for boolean properties
        elseif (preg_match('/^is([A-Z]\w*)/', $name, $matches))
        {
            return 'is';
        }
        // is a "yes" function? Only for boolean properties
        elseif (preg_match('/^yes([A-Z]\w*)/', $name, $matches))
        {
            return 'yes';
        }
        // is a "no" function? Only for boolean properties
        elseif (preg_match('/^no([A-Z]\w*)/', $name, $matches))
        {
            return 'no';
        }
        // is a to string date function?
        elseif (preg_match('/([a-z]\w*)Str$/', $name, $matches))
        {
            return 'str';
        }
        // is a "count" function?
        elseif (preg_match('/([a-z]\w*)Count$/', $name, $matches))
        {
            return 'count';
        }
        // is a "LastItem" function?
        elseif (preg_match('/([a-z]\w*)LastItem$/', $name, $matches))
        {
            return 'lastItem';
        }
        // is a "FirstItem" function?
        elseif (preg_match('/([a-z]\w*)FirstItem$/', $name, $matches))
        {
            return 'firstItem';
        }
        // unknown magic method
        return null;
    }

    /**
     * El mètode has i is que tots els properties tenen.
     *
     * @param string $name El nom del property
     * @param array $arguments Els paràmetres de la funció
     *
     * @return boolean|string|integer|null El valor de la funció.
     *
     * @throws DateFormatterInvalidStyleException
     * @throws ObjectUnknownMethodException
     */
    public function __call(string $name, array $arguments)
    {
        switch ($this->magicMethodSignatureEvaluator($name, $matches))
        {
            // is a "is" or "yes" function? Only for boolean properties
            case 'is':
            case 'yes':
            {
                $property = lcfirst($matches[1]);
                // exists and is a boolean property?
                if ($this->hasProperty($property) && $this->properties[$property][1] === Property::IS_BOOLEAN)
                {
                    return $this->$property === true;
                }
                break;
            }
            // is a "no" function? Only for boolean properties
            case 'no':
            {
                $property = lcfirst($matches[1]);
                // exists and is a boolean property?
                if ($this->hasProperty($property) && $this->properties[$property][1] === Property::IS_BOOLEAN)
                {
                    return $this->$property === false;
                }
                break;
            }
            // is a "has" function?
            case 'has':
            {
                $property = lcfirst($matches[1]);
                // exists and is not null?
                if ($this->hasProperty($property))
                {
                    // Exceptions: write-only? only internal ivars are acceptable
                    if ($this->properties[$property][0] === Property::WRITE_ONLY)
                    {
                        return isset($this->ivars[$property]) && ! is_null($this->ivars[$property]);
                    }
                    // specific has function
                    if (method_exists($this, $name))
                    {
                        return $this->$name();
                    }
                    elseif ($this->is_array_property($property)) // is an array?
                    {
                        return is_array($this->$property) && count($this->$property) > 0;
                    }
                    else // non array objects
                    {
                        return ! is_null($this->$property);
                    }
                }
                break;
            }
            // is a "count" function?
            case 'count':
            {
                $property = lcfirst($matches[1]);
                // specific count function
                if (method_exists($this, $name))
                {
                    return $this->$name();
                }
                // exists and is a array property?
                if ($this->hasProperty($property) && $this->is_array_property($property))
                {
                    return is_array($this->$property) ? count($this->$property) : 0;
                }
                break;
            }
            // is a "LastItem" function?
            case 'lastItem':
            {
                $property = lcfirst($matches[1]);
                // specific LastItem function
                if (method_exists($this, $name))
                {
                    return $this->$name();
                }
                // exists and is a array property?
                if ($this->hasProperty($property) && $this->is_array_property($property))
                {
                    if (is_array($this->$property) && $count = count($this->$property))
                    {
                        return $this->$property[$count - 1];
                    }
                    // no last item
                    return null;
                }
                break;
            }
            // is a "FirstItem" function?
            case 'firstItem':
            {
                $property = lcfirst($matches[1]);
                // specific FirstItem function
                if (method_exists($this, $name))
                {
                    return $this->$name();
                }
                // exists and is a array property?
                if ($this->hasProperty($property) && $this->is_array_property($property))
                {
                    return is_array($this->$property) && count($this->$property) > 0 ? $this->$property[0] : null;
                }
                break;
            }
            // is a to string date function?
            case 'str':
            {
                $property = lcfirst($matches[1]);
                // exists and is a date property?
                if ($this->hasProperty($property) && $this->properties[$property][1] === Property::IS_DATE)
                {
                    $argument1 = count($arguments) > 0 ? $arguments[0] : DateFormatter::DATE_TIME_STYLE;
                    $argument2 = count($arguments) > 1 ? $arguments[1] : 'es';
                    // use the date formatter to convert it to string
                    if (is_object($this->$property) && $this->$property instanceof DateTime)
                    {
                        return DateFormatter::dateToString($this->$property, $argument2, $argument1);
                    }
                    // no date
                    return null;
                }
                break;
            }
        }
        // if we are here, then is an unknown function
        $this->throwUnknownMethodException($name);
    }

    // Class information

    /**
     * Retorna la classe de l'objecte (inclou el namespace). DEPRECATED en cas d'utilitzar PHP 5.5 o superior!
     *
     * @return string El nom de la classe.
     * @deprecated
     */
    static public function __class(): string
    {
        return get_called_class();
    }

    /**
     * Retorna la classe de l'objecte (no inclou el namespace).
     *
     * @return string El nom de la classe sense el namespace.
     */
    static public function __className(): string
    {
        return ($last = strrchr($class = static::class, '\\')) === false ? $class : substr($last, 1);
    }

    /**
     * Retorna el namespace de l'objecte.
     *
     * @return string El namespace de la classe.
     */
    static public function __namespace(): string
    {
        $class = get_called_class();
        // return the current namespace
        return substr($class, 0, strrpos($class, '\\')).'\\';
    }

    /**
     * Retorna la classe de l'objecte (no inclou el namespace).
     *
     * @return string El nom de la classe sense el namespace.
     */
    public function getClassName(): string
    {
        return static::__className();
    }

    // Class constructor

    /** @ignore */
    public function __construct()
    {
        $this->properties();
    }

    // Nested properties

    /**
     * Obté el valor d'un property seguint la anotació dels punts, es a dir, es pot accedir al property d'un property d'un property...
     * També és possible accedir als elements d'un array, a partir del seu index o key.
     *
     * Exemple 1: property.childProperty
     * Exemple 2: property.arrayProperty.index.property
     *
     * @param string $nestedProperty Els properties que es volen obtenir.
     *
     * @return mixed El valor de l'últim property de la cadena.
     */
    public function getNestedPropertyValue(string $nestedProperty)
    {
        //
        // TODO: Millorar aquest mètode
        // Potencial bug: Quan hi ha algun . en algun paràmetre d'una funció pot causar problemes
        // Exemple: objecte.property.method("file.ext")
        //
        if ($properties = explode('.', $nestedProperty))
        {
            $value = null;
            $property = $properties[0];
            // is a valid property?
            if ($this->hasProperty($property))
            {
                $value = $this->$property;
            }
            elseif ($this->hasMethod($property))
            {
                $value = $this->evaluateMethod($property);
            }
            // has a value?
            if ($value !== null)
            {
                unset($properties[0]);
                // get the desired value
                while (count($properties) > 0)
                {
                    // clean-up the properties array
                    $properties = array_values($properties);
                    // call it recursive
                    if (is_object($value) && $value instanceof Objectum)
                    {
                        return $value->getNestedPropertyValue(implode('.', $properties));
                    }
                    elseif (is_array($value))
                    {
                        if (isset($value[$properties[0]]))
                        {
                            $value = $value[$properties[0]];
                            // remove the index key
                            unset($properties[0]);
                        }
                        else // invalid index
                        {
                            return null;
                        }
                    }
                    elseif (is_object($value))
                    {
                        $parts = explode('(', $properties[0], 2);
                        // determine the signature and params
                        $params = count($parts) === 2 ? str_getcsv(substr($parts[1], 0, strrpos($parts[1], ')', 0))) : [];
                        // execute the method
                        if (method_exists($value, $parts[0]))
                        {
                            $value = call_user_func_array([$value, $parts[0]], $params);
                        }
                        elseif (property_exists(get_class($value), $parts[0])) // we assume that it is a "property"
                        {
                            $prop = $parts[0];
                            $value = $value->$prop;
                        }
                        // remove the index key
                        unset($properties[0]);
                    }
                }
                return $value;
            }
        }
        return null;
    }

    // Class copying

    /**
     * Clona l'objecte actual a un altre tipus d'objecte.
     *
     * @param string $class La nova classe de l'objecte
     *
     * @return mixed L'objecte clonat
     */
    public function cloneAsClass(string $class)
    {
        $cloned = new $class;
        // copy each property
        foreach (array_intersect(array_keys(get_object_vars($this)), array_keys(get_object_vars($cloned))) as $property)
        {
            // if the destiny class is an Objectum then...
            if ($cloned instanceof Objectum)
            {
                // the "ivar properties" is not clonable
                if ($property === 'properties') continue;
                // keep only the properties which are present into cloned one
                if ($property === 'ivars')
                {
                    foreach ($this->ivars as $ivar => $value)
                    {
                        if ($cloned->hasProperty($ivar))
                        {
                            $cloned->ivars[$ivar] = $value;
                        }
                    }
                    continue;
                }
            }
            // assign it
            $cloned->$property = $this->$property;
        }
        return $cloned;
    }

    /**
     * Clona l'objecte a la classe pare.
     *
     * @return mixed L'objecte clonat
     */
    public function cloneAsSuperClassType()
    {
        return $this->cloneAsClass(get_parent_class($this));
    }

    // Properties

    /**
     * Comprovació standard de si un objecte està buit o no.
     *
     * @return boolean TRUE = Està buit, FALSE = pos no.
     */
    public function isEmpty(): bool
    {
        return count(array_filter((array)$this->ivars)) === 0;
    }

    /**
     * Compara la geolocalització actual amb una altre geolocalització.
     *
     * @param mixed $other El valor amb la que comparar.
     *
     * @return boolean TRUE = Son iguals els dos objectes, FALSE = no son iguals.
     */
    public function isEqual($other): bool
    {
        if (is_object($other) && get_called_class() === get_class($other))
        {
            $ivars1 = array_filter($this->ivars);
            $ivars2 = array_filter($other->ivars);
            // check both arrays count
            if (count($ivars1) !== count($ivars2))
            {
                return false;
            }
            // compare their values
            $diff = array_udiff_assoc($ivars1, $ivars2, function ($a, $b)
            {
                if (is_object($a) && $a instanceof Objectum)
                {
                    return ! $a->isEqual($b);
                }
                // basic compare
                return $a != $b;
            });
            return count($diff) === 0;
        }
        return false;
    }

    /**
     * Retorna si existeix un property amb aquest nom a l'objecte.
     *
     * @param string $property Nom del property a comprovar.
     *
     * @return boolean TRUE = Existeix un property amb aquest nom, FALSE = no existeix.
     */
    public function hasProperty(string $property): bool
    {
        return isset($this->properties[$property]);
    }

    /**
     * Retorna si existeix un mètode amb aquest nom a l'objecte.
     *
     * @param string $method Nom del mètode a comprovar.
     *
     * @return boolean TRUE = Existeix un mètode amb aquest nom, FALSE = no existeix.
     */
    public function hasMethod(string $method): bool
    {
        if (($signature = strtok($method, '(')) !== false)
        {
            if ( ! method_exists($this, $signature))
            {
                return $this->magicMethodSignatureEvaluator($signature) !== null;
            }
            return true;
        }
        return false;
    }

    /**
     * Evalúa i executa un mètode de l'objecte.
     *
     * @param string $method El mètode amb els paràmetres a parsejar i executar.
     *
     * @return mixed|void El resultat d'executar el mètode.
     */
    public function evaluateMethod(string $method)
    {
        $parts = explode('(', $method, 2);
        // determine the signature and params
        $params = str_getcsv(substr($parts[1], 0, strrpos($parts[1], ')', 0)));
        // execute the method
        return call_user_func_array([$this, $parts[0]], $params);
    }

    /**
     * Retorna si una propietat és d'escriptura i lectura.
     *
     * @param string $property La propietat que es vol comprovar.
     *
     * @return boolean TRUE = És només de lectura, FALSE = no.
     */
    public function isReadWriteProperty(string $property): bool
    {
        return isset($this->properties[$property]) && $this->properties[$property][0] === Property::READ_WRITE;
    }

    /**
     * Retorna si una propietat és només de lectura.
     *
     * @param string $property La propietat que es vol comprovar.
     *
     * @return boolean TRUE = És només de lectura, FALSE = no.
     */
    public function isReadOnlyProperty(string $property): bool
    {
        return isset($this->properties[$property]) && $this->properties[$property][0] === Property::READ_ONLY;
    }

    /**
     * Retorna si una propietat és d'escriptura i lectura.
     *
     * @param string $property La propietat que es vol comprovar.
     *
     * @return boolean TRUE = És només de lectura, FALSE = no.
     */
    public function isWriteOnlyProperty(string $property): bool
    {
        return isset($this->properties[$property]) && $this->properties[$property][0] === Property::WRITE_ONLY;
    }

    /**
     * Retorna si una propietat es pot canviar (Read-Write o Write-Only)
     *
     * @param string $property La propietat que es vol comprovar.
     *
     * @return boolean TRUE = Es pot canviar, FALSE = no.
     */
    public function isWritableProperty(string $property): bool
    {
        return $this->isReadWriteProperty($property) || $this->isWriteOnlyProperty($property);
    }

    /**
     * Retorna totes les propietats que es poden modificar (escriure).
     *
     * @return string[] Les propietats modificables.
     */
    public function writableProperties(): array
    {
        $properties = [];
        // filter writable properties
        foreach ($this->properties as $property => $info)
        {
            // for a better performance the isWritableProperty() is not used
            if ($this->properties[$property][0] === Property::READ_WRITE || $this->properties[$property][0] === Property::WRITE_ONLY)
            {
                $properties[] = $property;
            }
        }
        // the writable properties
        return $properties;
    }

    // Private key tools

    private function key(): string
    {
        return 'f9DQ:mxR9hz5_c8kmdCsjauikv#,G8;v';
    }

    // Internal tools

    protected function is_array_property(string $property): bool
    {
        $type = $this->properties[$property][1];
        // validate the type
        return $type === Property::IS_ARRAY || $type === Property::IS_ARRAY_ENUM;
    }

    /**
     * Assigna un property.
     *
     * NOTE: The "hasProperty($property) method is not used for better performance.
     *
     * @param string $property El nom del property que es vol assignar.
     * @param mixed $value El valor que es vol assignar.
     *
     * @throws
     */
    protected function set_ivar(string $property, $value): void
    {
        if (isset($this->properties[$property]))
        {
            $type = $this->properties[$property][1];
            // determine the type of property
            if ($type === Property::IS_INTEGER)
            {
                $value = is_null($value) ? null : (integer)$value;
            }
            elseif ($type === Property::IS_BOOLEAN)
            {
                if (is_string($value))
                {
                    $value = strtolower(trim($value));
                    $value = in_array($value, ['yes', 'true', 'on', 'si']);
                }
                else // direct casting to boolean
                {
                    $value = is_null($value) ? null : (boolean)$value;
                }
            }
            elseif ($type === Property::IS_FLOAT)
            {
                $value = is_null($value) ? null : (float)$value;
            }
            elseif ($type === Property::IS_DOUBLE)
            {
                $value = is_null($value) ? null : (double)$value;
            }
            elseif ($type === Property::IS_STRING || $type === Property::IS_STRING_ENCRYPTED)
            {
                if (is_array($value) || (is_object($value) && ! method_exists($value, '__toString')))
                {
                    $value = null;
                }
                else // is a "valid" string value
                {
                    $value = trim(str_replace(chr(226).chr(128).chr(168), "\n", strval($value)));
                    // is empty?
                    if (strlen($value) === 0)
                    {
                        $value = null;
                    }
                    elseif ($type === Property::IS_STRING_ENCRYPTED)
                    {
                        $key = sha1($this->string_encrypted_key.$this->key());
                        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
                        $value = openssl_encrypt($value, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv).$iv;
                    }
                }
            }
            elseif ($type === Property::IS_DATE)
            {
                $value = ($value instanceof DateTime) ? $value : null;
            }
            elseif ($type === Property::IS_ARRAY)
            {
                $value = is_array($value) ? $value : null;
            }
            elseif ($type === Property::IS_ARRAY_ENUM)
            {
                if ($value = is_array($value) ? $value : null)
                {
                    /** @var Enum $enum */
                    $enum = $this->properties[$property][2];
                    // validate each array element
                    if ( ! $enum::validateValues($value, $invalid))
                    {
                        $this->throwInvalidArrayEnumElementsException($property, $enum, $invalid);
                    }
                }
            }
            elseif ($type === Property::IS_DICTIONARY)
            {
                $value = is_array($value) ? $value : (is_object($value) && $value instanceof stdClass ? (array)$value : null);
            }
            elseif ($type === Property::IS_ENUM || $type === Property::IS_ENUM_NULLABLE)
            {
                /** @var Enum $enumName */
                $enumName = $this->properties[$property][2] ?? null;
                // validate the enum value
                if ($enumName && class_exists($enumName) && is_subclass_of($enumName, Enum::class))
                {
                    if ( ! $enumName::validateValue($value, $type === Property::IS_ENUM_NULLABLE))
                    {
                        $this->throwInvalidPropertyEnumValueException($property, $value, $enumName::values(), $enumName);
                    }
                }
                else // invalid enum class (or is null or is not a subclass of Enum)
                {
                    $this->throwInvalidPropertyEnumInstanceException($property, $enumName);
                }
            }
            elseif ($type === Property::IS_OBJECT || $type === Property::IS_OBJECT_NULLABLE)
            {
                // check for native types
                if ( ! is_null($value) && ! is_object($value))
                {
                    $objectName = $this->properties[$property][2] ?? Objectum::class;
                    // throw exception
                    $this->throwInvalidPropertyInstanceException($property, 'primitive type', $objectName);
                } // is a defined object?
                elseif (isset($this->properties[$property][2]))
                {
                    $classes = is_array($this->properties[$property][2]) ? $this->properties[$property][2] : [$this->properties[$property][2]];
                    $nullable = $this->properties[$property][1] === Property::IS_OBJECT_NULLABLE;
                    // is a null and are not allowed?
                    if (is_null($value) && ! $nullable)
                    {
                        $this->throwInvalidPropertyInstanceException($property, 'NULL', $this->properties[$property][2]);
                    }
                    else
                    {
                        $validValue = is_null($value) && $nullable;
                        // validate types
                        foreach ($classes as $class)
                        {
                            if (is_a($value, $class))
                            {
                                $validValue = true;
                                break;
                            }
                        }
                        // is not a valid value?
                        if ( ! $validValue)
                        {
                            $this->throwInvalidPropertyInstanceException($property, get_class($value), $this->properties[$property][2]);
                        }
                    }
                }
            }
            // assign the ivar value
            $this->ivars[$property] = $value;
        }
        else // error
        {
            $this->throwMissingPropertyException($property);
        }
    }

    /**
     * Afegeix un element a un property tipus Array.
     *
     * NOTE: The "hasProperty($property) method is not used for better performance.
     *
     * @param string $property El nom del property que té l'array al qui se li vol afegir el valor.
     * @param mixed $value El valor que es vol afegir. Pot ser un array, i llavors validarà cada element de dins de l'array.
     *
     * @throws
     */
    protected function push_ivar(string $property, $value): void
    {
        if (isset($this->properties[$property]))
        {
            if ($this->properties[$property][1] === Property::IS_ARRAY)
            {
                if (isset($this->properties[$property][2]))
                {
                    foreach (is_array($value) ? $value : [$value] as $item)
                    {
                        if ( ! is_object($item))
                        {
                            $objectName = isset($this->properties[$property][2]) ? $this->properties[$property][2] : Objectum::class;
                            // throw exception
                            $this->throwInvalidPropertyArrayInstanceException($property, 'primitive type', $objectName);
                        }
                        elseif (get_class($item) !== $this->properties[$property][2] && ! is_subclass_of($item, $this->properties[$property][2]))
                        {
                            $this->throwInvalidPropertyArrayInstanceException($property, get_class($item), $this->properties[$property][2]);
                        }
                        // add the value
                        $this->ivars[$property][] = $item;
                    }
                }
                else // is not an array of objects, so no validation is required
                {
                    $this->ivars[$property] = array_merge($this->ivars[$property] ?? [], is_array($value) ? $value : [$value]);
                }
            }
            elseif ($this->properties[$property][1] === Property::IS_ARRAY_ENUM)
            {
                /** @var Enum $enum */
                $enum = $this->properties[$property][2];
                // validate the value/s
                $value = is_array($value) ? $value : [$value];
                // trying to push invalid values??
                if ( ! $enum::validateValues($value, $invalid))
                {
                    $this->throwInvalidArrayEnumElementsException($property, $enum, $invalid);
                }
                else // all the values are valid
                {
                    $this->ivars[$property] = array_merge($this->ivars[$property] ?? [], $value);
                }
            }
            else // is not an array
            {
                $this->throwPropertyIsNotAnArrayException($property);
            }
        }
        else // error
        {
            $this->throwMissingPropertyException($property);
        }
    }

    /**
     * Elimina i retorna un element d'un property tipus Array (actualitza els index de l'array).
     *
     * NOTE: The "hasProperty($property) method is not used for better performance.
     *
     * @param string $property El nom del property que té l'array al qui se li vol eliminar l'ítem.
     * @param integer $index L'index del valor que es vol eliminar.
     *
     * @return mixed El valor extret de l'array.
     *
     * @throws
     */
    protected function pop_ivar(string $property, int $index)
    {
        if (isset($this->properties[$property]))
        {
            if ($this->is_array_property($property))
            {
                $value = $this->ivars[$property][$index];
                // remove it from the array
                unset($this->ivars[$property][$index]);
                // re-arrange array indexes
                $this->ivars[$property] = array_values($this->ivars[$property]);
                // return the popped value
                return $value;
            }
            else // is not an array
            {
                $this->throwPropertyIsNotAnArrayException($property);
            }
        }
        // error
        $this->throwMissingPropertyException($property);
        // this return will never fire...
        return false;
    }

    /**
     * Mou un item d'una posició a una altra d'un property tipus Array.
     *
     * NOTE: The "hasProperty($property) method is not used for better performance.
     *
     * @param string $property El nom del property que té l'array al qui se li vol moure l'ítem.
     * @param integer $from La posició inicial (el que es vol moure).
     * @param integer $to La nova posició (on es vol moure).
     *
     * @throws
     */
    protected function move_ivar(string $property, int $from, int $to)
    {
        if (isset($this->properties[$property]))
        {
            if ($this->is_array_property($property))
            {
                $out = array_splice($this->ivars[$property], $from, 1);
                array_splice($this->ivars[$property], $to, 0, $out);
            }
            else // is not an array
            {
                $this->throwPropertyIsNotAnArrayException($property);
            }
        }
        else // error
        {
            $this->throwMissingPropertyException($property);
        }
    }

    /**
     * Obté el valor d'un property codificat.
     *
     * @param string $property La propietat a descodificar.
     *
     * @return null|string El text descodificat.
     *
     * @throws
     */
    protected function get_encrypted_ivar(string $property): ?string
    {
        if (isset($this->properties[$property]))
        {
            if ($this->properties[$property][1] === Property::IS_STRING_ENCRYPTED)
            {
                if (isset($this->ivars[$property]))
                {
                    $key = sha1($this->string_encrypted_key.$this->key());
                    $iv = substr($this->ivars[$property], -openssl_cipher_iv_length('aes-256-cbc'));
                    $value = substr($this->ivars[$property], 0, -openssl_cipher_iv_length('aes-256-cbc'));
                    // try to decrypt
                    if (($decrypted = openssl_decrypt($value, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv)) !== false)
                    {
                        return $decrypted;
                    }
                }
                return null;
            }
            else // is not an encrypted string
            {
                $this->throwPropertyIsNotAnEncryptedStringException($property);
            }
        }
        else // error
        {
            $this->throwMissingPropertyException($property);
        }
        // this is a "return" for convenience
        return null;
    }

    // Properties exceptions

    /**
     * Genera l'excepció de que el property no és un array.
     *
     * @param string $property El property que ha generat l'excepció.
     *
     * @throws ObjectPropertyIsNotAnArrayException
     */
    protected function throwPropertyIsNotAnArrayException(string $property): void
    {
        throw new ObjectPropertyIsNotAnArrayException($property, $this);
    }

    /**
     * Genera l'excepció de que el property no és un string encriptat.
     *
     * @param string $property El property que ha generat l'excepció.
     *
     * @throws ObjectPropertyIsNotAnEncryptedStringException
     */
    protected function throwPropertyIsNotAnEncryptedStringException(string $property): void
    {
        throw new ObjectPropertyIsNotAnEncryptedStringException($property, $this);
    }

    /**
     * Genera l'excepció de que el property no existeix.
     *
     * @param string $property El property que ha generat l'excepció.
     *
     * @throws ObjectMissingPropertyException
     */
    protected function throwMissingPropertyException(string $property): void
    {
        throw new ObjectMissingPropertyException($property, $this);
    }

    /**
     * Genera l'excepció de que el property no té un alias vàlid.
     *
     * @param string $property El property que ha generat l'excepció.
     *
     * @throws ObjectInvalidPropertyAliasException
     */
    protected function throwInvalidPropertyAliasException(string $property): void
    {
        throw new ObjectInvalidPropertyAliasException($property, $this);
    }

    /**
     * Genera l'excepció de que el property és només de lectura.
     *
     * @param string $property El property que ha generat l'excepció.
     *
     * @throws ObjectReadOnlyException
     */
    protected function throwReadOnlyException(string $property): void
    {
        throw new ObjectReadOnlyException($property, $this);
    }

    /**
     * Genera l'excepció de que el property és només d'escriptura.
     *
     * @param string $property El property que ha generat l'excepció.
     *
     * @throws ObjectWriteOnlyException
     */
    protected function throwWriteOnlyException(string $property): void
    {
        throw new ObjectWriteOnlyException($property, $this);
    }

    /**
     * Genera l'excepció de que el property del tipus Enum no se li ha assignat un valor vàlid.
     *
     * @param string $property El property que ha generat l'excepció.
     * @param string|int|null $value El valor que s'ha intentat assignar.
     * @param array $values Els possibles valors.
     * @param string $enumName El enum.
     *
     * @throws ObjectInvalidPropertyEnumValueException
     */
    protected function throwInvalidPropertyEnumValueException(string $property, $value, array $values, string $enumName): void
    {
        throw new ObjectInvalidPropertyEnumValueException($property, $value, $values, $enumName);
    }

    /**
     * Genera l'excepció de que al property no se li ha assignat un Enum.
     *
     * @param string $property El property que ha generat l'excepció.
     * @param string $enumName El nom de l'enum.
     *
     * @throws ObjectInvalidPropertyEnumInstanceException
     */
    protected function throwInvalidPropertyEnumInstanceException(string $property, string $enumName): void
    {
        throw new ObjectInvalidPropertyEnumInstanceException($property, $enumName, $this);
    }

    /**
     * Genera l'excepció de que al property no se li ha assignat un objecte de la instancia esperada.
     *
     * @param string $property El property que ha generat l'excepció.
     * @param string $obj El que se li ha passat.
     * @param string $expected L'esperat.
     *
     * @throws ObjectInvalidPropertyInstanceException
     */
    protected function throwInvalidPropertyInstanceException(string $property, string $obj, string $expected): void
    {
        throw new ObjectInvalidPropertyInstanceException($property, $obj, $expected, $this);
    }

    /**
     * Genera l'excepció de que al property no se li ha assignat un array.
     *
     * @param string $property El property que ha generat l'excepció.
     * @param string $obj El que se li ha passat.
     * @param string $expected L'esperat.
     *
     * @throws ObjectInvalidPropertyArrayInstanceException
     */
    protected function throwInvalidPropertyArrayInstanceException(string $property, string $obj, string $expected): void
    {
        throw new ObjectInvalidPropertyArrayInstanceException($property, $obj, $expected, $this);
    }

    /**
     * Quan no existeix el mètode.
     *
     * @param string $method El mètode que no existeix.
     *
     * @throws ObjectUnknownMethodException
     */
    protected function throwUnknownMethodException(string $method): void
    {
        throw new ObjectUnknownMethodException($method, $this);
    }

    /**
     * Quan un array limitat per un enum conté items invàlids.
     *
     * @param string $property El property que ha generat l'excepció.
     * @param string $enum L'enum al que fa referència.
     * @param array $invalid Els valors que no són vàlids.
     *
     * @throws ObjectInvalidArrayEnumElementsException
     */
    protected function throwInvalidArrayEnumElementsException(string $property, string $enum, array $invalid): void
    {
        throw new ObjectInvalidArrayEnumElementsException($property, $enum, $invalid, $this);
    }
}
