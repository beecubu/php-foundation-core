<?php

namespace Beecubu\Foundation\Core;

/**
 * Diferents configuracions dels custom properties.
 */
class CustomProperty
{
    const ON_EXPORT    = 0x01;
    const ON_IMPORT    = 0x02;
    const ON_IMPORT_AND_EXPORT = self::ON_EXPORT | self::ON_IMPORT;

    const ON_RAW_MODE  = 0x04;
    const ON_JSON_MODE = 0x08;
    const ON_ALL_MODES = self::ON_RAW_MODE | self::ON_JSON_MODE;

    const ON_IMPORT_AND_EXPORT_IN_ALL_MODES = self::ON_IMPORT_AND_EXPORT | self::ON_ALL_MODES;
}