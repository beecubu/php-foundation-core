<?php

/** @noinspection PhpUnusedParameterInspection */

namespace Beecubu\Foundation\Core\Exceptions;

use Exception;

/**
 * Quan el property s'ha definit com a Enum però el tipus definit no és un Enum.
 */
class ObjectInvalidPropertyEnumValueException extends Exception
{
    public function __construct(string $property, $value, array $values, string $enumName)
    {
        parent::__construct("Error: The value '".($value ?? 'null')."' is not valid for enum '{$enumName}'. Expected values are [".implode(',', $values)."].");
    }
}