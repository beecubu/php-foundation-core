<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan un array limitat per un enum conté items invàlids.
 */
class ObjectInvalidArrayEnumElementsException extends Exception
{
    public function __construct(string $property, string $enum, array $invalid, Objectum $who)
    {
        parent::__construct("Error: Property array '".$property."' only accept values of Enum '".$enum."' (invalid detected values are: ".implode(',', $invalid).") for class ".get_class($who).'".');
    }
}