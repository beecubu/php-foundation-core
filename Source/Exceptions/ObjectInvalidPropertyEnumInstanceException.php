<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property s'ha definit com a Enum però el tipus definit no és un Enum.
 */
class ObjectInvalidPropertyEnumInstanceException extends Exception
{
    public function __construct(string $property, string $enumName, Objectum $who)
    {
        parent::__construct("Error: Property '{$property}' must be an instance of 'Enum' and an instance of '{$enumName}' is given for class '".get_class($who)."'");
    }
}