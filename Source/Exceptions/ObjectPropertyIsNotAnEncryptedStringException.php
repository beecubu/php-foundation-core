<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property al que s'intenta accedir no és del tipus string codificat.
 */
class ObjectPropertyIsNotAnEncryptedStringException extends Exception
{
    public function __construct(string $property, Objectum $who)
    {
        parent::__construct("Error: Property '".$property."' is not an encrypted string property for class '".get_class($who)."'");
    }
}
