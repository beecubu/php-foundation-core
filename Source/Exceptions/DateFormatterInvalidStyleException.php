<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Exception;

/**
 * Quan l'estil de data no és vàlid.
 */
class DateFormatterInvalidStyleException extends Exception
{
    public function __construct(string $style)
    {
        parent::__construct("Error: '$style' is not a valid DateFormatter info value");
    }
}