<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Exception;

/**
 * Quan l'objecte no implementa el mètode instanceByRawDataId.
 */
class SerializableDontImplementsDefaultFactoryException extends Exception
{
    public function __construct(string $class)
    {
        parent::__construct("The class '$class' doesn't implements the instanceByRawDataId method.");
    }
}