<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property que s'intenta canviar és només de lectura.
 */
class ObjectReadOnlyException extends Exception
{
    public function __construct(string $property, Objectum $who)
    {
        parent::__construct("Error: Property '".$property."' is read-only for class '".get_class($who)."'");
    }
}