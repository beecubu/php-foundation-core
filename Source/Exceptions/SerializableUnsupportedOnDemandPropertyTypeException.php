<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Exception;

/**
 * Quan s'intenta fer un on-demand a un property que no és ni un ARRAY o OBJECT.
 */
class SerializableUnsupportedOnDemandPropertyTypeException extends Exception
{
    public function __construct(string $class, string $property)
    {
        parent::__construct("The '$property' property of class '$class' is not compatible with the on-demand feature.");
    }
}