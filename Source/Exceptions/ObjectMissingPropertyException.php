<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property al que s'intenta accedir no forma part de l'objecte.
 */
class ObjectMissingPropertyException extends Exception
{
    public function __construct(string $property, Objectum $who)
    {
        parent::__construct("Error: Property '".$property."' is not part of class '".get_class($who)."'");
    }
}
