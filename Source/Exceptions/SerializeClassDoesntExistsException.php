<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Serializable;
use Exception;

/**
 * Quan no existeix la classe que s'ha definit a un property.
 */
class SerializeClassDoesntExistsException extends Exception
{
    public function __construct(string $class, ?string $property = null, ?Serializable $who = null)
    {
        if ( ! $property || ! $who)
        {
            parent::__construct("The class '".$class."' doesn't exists.");
        }
        else // regular constructor
        {
            parent::__construct("The class '".$class."' defined in property '".$property."' doesn't exists for class '".get_class($who)."'.");
        }
    }
}


