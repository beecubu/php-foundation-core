<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Serializable;
use Exception;

/**
 * Quan el Serialize no sap com crear un objecte ja que hi ha multiples definicions.
 */
class SerializeUnknownHowToUnserializePropertyException extends Exception
{
    public function __construct(string $property, Serializable $who)
    {
        parent::__construct("More than one class is defined for property '".$property."' for class '".get_class($who)."'. Please use the exceptions for handle this.");
    }
}
