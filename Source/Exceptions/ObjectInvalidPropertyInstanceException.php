<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property que s'intenta assignar no és del tipus permès pel property.
 */
class ObjectInvalidPropertyInstanceException extends Exception
{
    public function __construct(string $property, string $obj, string $expected, Objectum $who)
    {
        parent::__construct("Error: Property '".$property."' must be an instance of '".$expected."' and an instance of '".$obj."' is given for class '".get_class($who)."'");
    }
}