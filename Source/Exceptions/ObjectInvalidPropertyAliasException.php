<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property del tipus 'IS_PROPERTY_ALIAS' no té definit l'alias.
 */
class ObjectInvalidPropertyAliasException extends Exception
{
    public function __construct(string $property, Objectum $who)
    {
        parent::__construct("Error: Property '".$property."' of type IS_PROPERTY_ALIAS has no alias defined for class '".get_class($who)."'");
    }
}