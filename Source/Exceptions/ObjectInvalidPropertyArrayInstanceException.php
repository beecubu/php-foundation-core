<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property que s'intenta assignar no és del tipus permès pel property.
 */
class ObjectInvalidPropertyArrayInstanceException extends Exception
{
    public function __construct(string $property, string $obj, string $expected, Objectum $who)
    {
        parent::__construct("Error: Property array '".$property."' only accepts instances of '".$expected."' (trying to add an instance of '".$obj."' for class '".get_class($who)."').");
    }
}