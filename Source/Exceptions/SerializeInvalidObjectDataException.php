<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Serializable;
use Exception;

/**
 * Quan el data del Serialize no és informació correcte per serialitzar.
 */
class SerializeInvalidObjectDataException extends Exception
{
    public function __construct(Serializable $who)
    {
        parent::__construct("The data provided to import an object of type '".get_class($who)."' is not valid, a primitive value has been passed instead.");
    }
}
