<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Serializable;
use Exception;

/**
 * Quan l'objecte que s'intenta desserialitzar no hereda de Serializable.
 */
class SerializeIsNotSubclassOfSerializableException extends Exception
{
    public function __construct(string $class, ?Serializable $who = null)
    {
        if ( ! $who)
        {
            parent::__construct("The class '".$class."' is not a subclass of '".Serializable::class.".");
        }
        else // regular constructor
        {
            parent::__construct("The class '".$class."' is not a subclass of '".Serializable::class."' for class '".get_class($who)."'.");
        }
    }
}






