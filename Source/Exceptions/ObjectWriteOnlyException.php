<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan el property que s'intenta llegir el seu valor és només d'escriptura.
 */
class ObjectWriteOnlyException extends Exception
{
    public function __construct(string $property, Objectum $who)
    {
        parent::__construct("Error: Property '".$property."' is write-only for class '".get_class($who)."'");
    }
}
