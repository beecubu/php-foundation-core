<?php

namespace Beecubu\Foundation\Core\Exceptions;

use Beecubu\Foundation\Core\Objectum;
use Exception;

/**
 * Quan s'intenta executar un mètode que no forma part de la classe.
 */
class ObjectUnknownMethodException extends Exception
{
    public function __construct(string $method, Objectum $who)
    {
        parent::__construct("Error: Method '".$method."()' is not part of class '".get_class($who)."'");
    }
}
