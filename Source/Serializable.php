<?php

/** @noinspection PhpUnusedParameterInspection */

namespace Beecubu\Foundation\Core;

use Beecubu\Foundation\Core\Exceptions\ObjectInvalidPropertyAliasException;
use Beecubu\Foundation\Core\Exceptions\ObjectMissingPropertyException;
use Beecubu\Foundation\Core\Exceptions\ObjectWriteOnlyException;
use Beecubu\Foundation\Core\Exceptions\SerializableDontImplementsDefaultFactoryException;
use Beecubu\Foundation\Core\Exceptions\SerializableUnsupportedOnDemandPropertyTypeException;
use Beecubu\Foundation\Core\Exceptions\SerializeClassDoesntExistsException;
use Beecubu\Foundation\Core\Exceptions\SerializeInvalidObjectDataException;
use Beecubu\Foundation\Core\Exceptions\SerializeIsNotSubclassOfSerializableException;
use Beecubu\Foundation\Core\Exceptions\SerializeUnknownHowToUnserializePropertyException;
use Beecubu\Foundation\Core\Tools\DateTime;
use Beecubu\Foundation\Core\Tools\Helpers;
use JsonSerializable;
use stdClass;

/**
 * Implementa les funcionalitats bàsiques dels objectes serializables.
 *
 * @package Foundation\Serializable
 *
 * @method boolean hasId()
 */
abstract class Serializable extends Objectum implements \Serializable, JsonSerializable
{
    // static config property
    protected static $ivar_publicId = 'id';

    // internal id
    protected static $ivar_internalId = 'id';
    public static    $TimestampFactor = 1;

    // json configuration
    protected $excJsonProp = [];
    protected $oveJsonProp = [];

    // serialization exceptions
    protected $propertyExceptions = [];
    protected $customProperties   = [];

    // auto-serialization class name
    protected $serializeClassName     = false; // true = an _class key is appended with the full class name
    protected $serializeFullClassName = true; // true = appends the namespace (recommended) - WARNING if this value is FALSE then the class must be in the same namespace to work properly

    // stores lazy load ivars
    protected $ivarsOnDemand = [];

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append the new properties to ignore
        $this->properties[static::$ivar_publicId] = [Property::READ_ONLY, Property::IS_STRING];
    }

    /**
     * Determina si un objecte és del tipus DateTime RawData.
     *
     * @param mixed $value El valor a comprovar.
     *
     * @return boolean TRUE = Ho és, FALSE = no.
     */
    final protected function isRawDateTimeInstance($value): bool
    {
        return static::__isRawDateTimeInstance($value);
    }

    /**
     * Determina si un objecte és del tipus MongoBinData RawData.
     *
     * @param mixed $value El valor a comprovar.
     *
     * @return boolean TRUE = Ho és, FALSE = no.
     */
    final protected function isBinaryDataInstance($value): bool
    {
        return static::__isBinaryDataInstance($value);
    }

    protected function overriddenJsonProperties(): void
    {
        // the json properties to override (replace) should be declared in sub-classes of Object
    }

    protected function excludedJsonProperties(): void
    {
        // the json properties to exclude should be declared in sub-classes of Object
    }

    protected function customProperties(): void
    {
        // the properties to be processed specially when serializing/unserializing
    }

    /**
     * @param string $property El property que s'està deserialitzant.
     * @param mixed $value El valor tal qual s'ha llegit del JSON/RawData.
     * @param bool $replace
     * @param boolean $rawDataMode TRUE = S'està serialitzant a RawData, FALSE = ha JSON.
     * @param bool $writableOnly
     *
     * @return mixed El valor del property tractat.
     */
    protected function importCustomProperty(string $property, $value, bool $replace = true, bool $rawDataMode = false, bool $writableOnly = false)
    {
        return $value;
    }

    protected function propertySerializeExceptions(): void
    {
        // the properties to be processed specially when serializing/unserializing
    }

    public function __construct()
    {
        parent::__construct();
        // configure json
        $this->excludedJsonProperties();
        $this->overriddenJsonProperties();
        // initialize property exceptions
        $this->customProperties();
        $this->propertySerializeExceptions();
    }

    // Class constructor

    /**
     * El mètode bàsic per poder utilitzar la funcionalitat dels referenced-objects.
     *
     * @param mixed $id L'id de l'objecte a crear.
     *
     * @return mixed
     *
     * @throws SerializableDontImplementsDefaultFactoryException
     */
    protected static function instanceByRawDataId($id)
    {
        throw new SerializableDontImplementsDefaultFactoryException(static::class);
    }

    /**
     * Valida i converteix (si és necessari) la informació a guardar dins de "ivarsOnDemand".
     *
     * @param mixed $data La informació a guardar dins al "ivarsOnDemand"
     *
     * @return mixed
     */
    protected static function parseDataBeforeInsertInIvarsOnDemand($data)
    {
        return $data;
    }

    // Protected methods

    /**
     * Converteix un property normal a Id.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param mixed $id El valor del property a convertir.
     *
     * @return mixed El property convertit a Id.
     */
    protected static function __idToRawData($id)
    {
        return $id;
    }

    /**
     * Converteix un Id a un property.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param mixed $id L'id a convertir a property.
     *
     * @return mixed L'id convertit a property.
     */
    protected static function __idFromRawData($id)
    {
        return $id;
    }

    /**
     * Converteix un DateTime al format necessari en RawData.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param \DateTime $dateTime La data a convertir.
     *
     * @return mixed La data convertida.
     */
    protected static function __dateTimeToRawData(\DateTime $dateTime)
    {
        return $dateTime->getTimestamp();
    }

    /**
     * Converteix un valor dateTime en format RawData a DateTime.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param mixed $dateTime El valor a convertir a DateTime.
     *
     * @return DateTime El valor convertit a DateTime.
     *
     * @throws
     */
    protected static function __dateTimeFromRawData($dateTime): DateTime
    {
        return new DateTime('@'.$dateTime);
    }

    /**
     * Determina si un objecte és del tipus DateTime RawData.
     *
     * @param mixed $value El valor a comprovar.
     *
     * @return boolean TRUE = Ho és, FALSE = no.
     */
    protected static function __isRawDateTimeInstance($value): bool
    {
        return $value instanceof \DateTime;
    }

    /**
     * Converteix informació binària al format necessari en RawData.
     *
     * @param mixed $data La informació a convertir en string.
     *
     * @return mixed La informació convertida.
     */
    protected static function __binaryDataToRawData($data)
    {
        if (static::__isBinaryDataInstance($data))
        {
            return base64_encode($data);
        }
        return null;
    }

    /**
     * Converteix un valor string a base64 (lol).
     *
     * @param mixed $data L'string a convertir a base64.
     *
     * @return mixed El valor convertit a mixed.
     */
    protected static function __binaryDataFromRawData($data)
    {
        if (is_string($data))
        {
            return base64_decode($data);
        }
        return null;
    }

    /**
     * Determina si un objecte és del tipus MongoBinData RawData.
     *
     * @param mixed $value El valor a comprovar.
     *
     * @return boolean TRUE = Ho és, FALSE = no.
     */
    protected static function __isBinaryDataInstance($value): bool
    {
        return ! is_null($value);
    }

    /**
     * Obté la instància d'un objecte, analitzant el data i fent ús de la informació del property.
     *
     * @param array|stdClass $data La informació de l'objecte en rawData.
     * @param array|null $property La definició del property ('name', 'read-write', 'type', etc...]).
     * @param Serializable|null $who L'objecte que ha cridat aquest mètode.
     *
     * @return string La classe.
     *
     * @throws SerializeClassDoesntExistsException
     * @throws SerializeIsNotSubclassOfSerializableException
     * @throws SerializeUnknownHowToUnserializePropertyException
     */
    private static function guessClassName($data, ?array $property = null, ?Serializable $who = null): string
    {
        $class = null;
        // determine the class to use
        if (is_array($data) && isset($data['_class']))
        {
            $class = $data['_class'];
            // we don't need it anymore
            unset($data['_class']);
        }
        elseif ($data instanceof stdClass && isset($data->_class))
        {
            $class = $data->_class;
            // we don't need it anymore
            unset($data->_class);
        }
        elseif ($property && isset($property[3]))
        {
            $class = $property[3];
        }
        elseif ( ! $property)
        {
            $class = static::class;
        }
        // impossible know how to import the property (multiple possibilities are present)
        if ( ! $class || is_array($class) && count($class) !== 1)
        {
            throw new SerializeUnknownHowToUnserializePropertyException($property[0] ?? 'unknown', $who);
        }
        // get the current namespace
        $namespace = static::__namespace();
        // should append a namespace?
        if ($namespace !== '\\' && substr_count($class, '\\') === 0)
        {
            $class = $namespace.$class;
        }
        // the class exists??
        if ( ! class_exists($class))
        {
            throw new SerializeClassDoesntExistsException($class, $property[0], $who);
        }
        elseif ( ! is_subclass_of($class, __CLASS__))
        {
            throw new SerializeIsNotSubclassOfSerializableException($class, $who);
        }
        // return the class name
        return $class;
    }

    /**
     * Crea un nou objecte serialitzable a partir de dades crues.
     *
     * @param array|stdClass $json Les dades cures a tractar.
     * @param boolean $replace TRUE = Els properties no definits al json s'esborraran, FALSE = els properties no definits al json es mantindran a l'objecte (si aquests estan definits).
     * @param boolean $writableOnly TRUE = Només els properties que són READ_WRITE o WRITE_ONLY són els que s'assignen (utilitzant els setters), la resta no.
     *
     * @return static La instància creada a partir de les dades crues.
     *
     * @throws
     */
    public static function instanceWithJson($json, bool $replace = true, bool $writableOnly = false)
    {
        $obj = new static();
        $obj->fromJson($json, $replace, $writableOnly);
        return $obj;
    }

    /**
     * Crea un nou objecte serialitzable a partir de dades crues.
     *
     * @param array|stdClass $rawData Les dades cures a tractar.
     * @param boolean $replace TRUE = Els properties no definits al rawData s'esborraran, FALSE = els properties no definits al rawData es mantindran a l'objecte (si aquests estan definits).
     * @param boolean $writableOnly TRUE = Només els properties que són READ_WRITE o WRITE_ONLY són els que s'assignen (utilitzant els setters), la resta no.
     *
     * @return static La instància creada a partir de les dades crues.
     *
     * @throws
     */
    public static function instanceWithRawData($rawData, bool $replace = true, bool $writableOnly = false)
    {
        // due to a zephir bug/limitation, static() cannot be used...
        /** @type static $obj */
        $class = static::guessClassName($rawData);
        $obj = new $class();
        $obj->fromRawData($rawData, $replace, $writableOnly);
        return $obj;
    }

    /**
     * Converteix un property normal a Id.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param mixed $id El valor del property a convertir.
     *
     * @return mixed El property convertit a Id.
     */
    final protected function idToRawData($id)
    {
        return static::__idToRawData($id);
    }

    /**
     * Converteix un Id a un property.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param mixed $id L'id a convertir a property.
     *
     * @return mixed L'id convertit a property.
     */
    final protected function idFromRawData($id)
    {
        return static::__idFromRawData($id);
    }

    /**
     * Converteix un DateTime al format necessari en RawData.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param \DateTime $dateTime La data a convertir.
     *
     * @return mixed La data convertida.
     */
    final protected function dateTimeToRawData(\DateTime $dateTime)
    {
        return static::__dateTimeToRawData($dateTime);
    }

    /**
     * Converteix un valor dateTime en format RawData a DateTime.
     * Aquest mètode en aquesta classe no té sentit, hi és només per conveniencia.
     *
     * @param mixed $dateTime El valor a convertir a DateTime.
     *
     * @return DateTime El valor convertit a DateTime.
     *
     * @throws
     */
    final protected function dateTimeFromRawData($dateTime): DateTime
    {
        return static::__dateTimeFromRawData($dateTime);
    }

    /**
     * Converteix informació binària al format necessari en RawData.
     *
     * @param mixed $data La informació a convertir en string.
     *
     * @return mixed La informació convertida.
     */
    final protected function binaryDataToRawData($data)
    {
        return static::__binaryDataToRawData($data);
    }

    /**
     * Converteix un valor string a base64 (lol).
     *
     * @param mixed $data L'string a convertir a base64.
     *
     * @return mixed El valor convertit a mixed.
     */
    final protected function binaryDataFromRawData($data)
    {
        return static::__binaryDataFromRawData($data);
    }

    /**
     * Aquest mètode es crida en el moment que un property s'ha marcat com a tractament especial (excepció) i s'està convertint a JSON/RawData.
     *
     * @param string $property El property que s'està serialitzant.
     * @param mixed $value El valor tal qual està just abans de serialitzar.
     * @param boolean $rawDataMode TRUE = S'està serialitzant a RawData, FALSE = ha JSON.
     *
     * @return mixed El valor del property tractat.
     *
     * @deprecated Utilitzar: exportCustomProperty(...)
     */
    protected function serializingSpecialProperty(string $property, $value, bool $rawDataMode)
    {
        return $value;
    }

    /**
     * Aquest mètode es crida en el moment que un property s'ha marcat com a tractament especial (excepció) i s'està convertint a JSON/RawData.
     *
     * @param string $property El property que s'està serialitzant.
     * @param mixed $value El valor tal qual està just abans de serialitzar.
     * @param bool $rawDataMode TRUE = S'està serialitzant a RawData, FALSE = ha JSON.
     * @param array|null $fields Els properties que es volen excloure. També es poden excloure fills d'aquest objecte.
     * @param bool $excluding TRUE = Els valors null o empty (exemple un array buit) no s'afegeixen, FALSE = S'afegeixen.
     * @param bool $ignoreEmptyValues TRUE = Els valors null o empty (exemple un array buit) no s'afegeixen, FALSE = S'afegeixen.
     *
     * @return mixed El valor del property tractat.
     */
    protected function exportCustomProperty(string $property, $value, bool $rawDataMode, ?array $fields = null, bool $excluding = true, bool $ignoreEmptyValues = true)
    {
        return $value;
    }

    /**
     * Aquest mètode es crida en el moment que un property s'ha marcat com a tractament especial (excepció) i s'està llegint d'un JSON/RawData.
     *
     * @param string $property El property que s'està deserialitzant.
     * @param mixed $value El valor tal qual s'ha llegit del JSON/RawData.
     * @param boolean $rawDataMode TRUE = S'està serialitzant a RawData, FALSE = ha JSON.
     *
     * @return mixed El valor del property tractat.
     *
     * @deprecated Utilitzar: importCustomProperty(...)
     */
    protected function unserializingSpecialProperty(string $property, $value, bool $rawDataMode)
    {
        return $value;
    }

    /** @inheritDoc */
    protected function set_ivar(string $property, $value): void
    {
        // execute the normal flow
        parent::set_ivar($property, $value);
        // this property is an "on-demand" property?
        if (isset($this->properties[$property][3]) && $this->properties[$property][3] === SerializableProperty::CREATE_ON_DEMAND_BY_ID)
        {
            unset($this->ivarsOnDemand[$property]);
        }
    }

    /**
     * Converteix un objecte de forma recursiva a json (stdClass)
     *
     * @param array|null $fields Els properties que es volen excloure. També es poden excloure fills d'aquest objecte.
     * @param bool $excluding TRUE = Els properties definits a "$fields" son els que s'exclouran, FALSE = els properties a "$fields" són els unics que sortiran.
     * @param boolean $ignoreEmptyValues TRUE = Els valors null o empty (exemple un array buit) no s'afegeixen, FALSE = S'afegeixen.
     * @param boolean $rawDataMode TRUE = Converteix els IDs (si és necessari) i altres conversions, FALSE = els ids els tracta com properties "normals".
     *
     * @return stdClass L'objecte en format json.
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    protected function exportProperties(?array $fields = null, bool $excluding = true, bool $ignoreEmptyValues = true, bool $rawDataMode = false): stdClass
    {
        $result = new stdClass();
        // is the auto-serialize Class Name active?
        if ($this->serializeClassName && $rawDataMode)
        {
            $result->_class = $this->serializeFullClassName ? static::class : static::__className();
        }
        // determine which values use to encode (ivars or properties)
        $valuesDataSet = array_keys($rawDataMode ? $this->ivars : $this->properties);
        // append the "not used" on demand values (only in rawData mode)
        if ($rawDataMode && ! empty($this->ivarsOnDemand))
        {
            $result = (object)array_merge((array)$result, $this->ivarsOnDemand);
        }
        // properties to exclude
        $excJsonProp = $rawDataMode ? [] : (array)$this->excJsonProp;
        $excludedFields = ($rawDataMode || ! $excluding ? [] : $fields) ?? [];
        // ensure $fields is initialized
        $fields = $fields ?? [];
        $hasFields = count($fields) > 0;
        $fieldsKeys = [];
        array_walk($fields, function ($value, $key) use (&$fieldsKeys)
        {
            $fieldsKeys[] = is_int($key) ? $value : $key;
        });
        // determine the properties to export
        $properties = array_udiff($valuesDataSet, array_merge($excludedFields, $excJsonProp), function ($a, $b)
        {
            return strcmp(is_array($a) ? '' : $a, is_array($b) ? '' : $b);
        });
        // parse all ivars (properties)
        foreach ($properties as $property)
        {
            // is a write-only property? then in json mode this key will not be present
            if ( ! $rawDataMode && $this->isWriteOnlyProperty($property))
            {
                continue;
            }
            // get the final property name (using the overridden json keys map)
            $key = ! $rawDataMode && isset($this->oveJsonProp[$property]) ? $this->oveJsonProp[$property] : $property;
            // we are filtering fields and this property isn't in "fields" list?
            if ( ! $rawDataMode && ! $excluding && $hasFields && ! in_array($key, $fieldsKeys))
            {
                continue;
            }
            // get the value (from ivars when using rawDataMode and from property when not rawDataMode)
            $value = $rawDataMode ? $this->ivars[$property] : $this->$property;
            // the current key should be handled specially?
            if (array_key_exists($property, $this->customProperties))
            {
                $conf = $this->customProperties[$property];
                $mode = $rawDataMode ? CustomProperty::ON_RAW_MODE : CustomProperty::ON_JSON_MODE;
                // should be it processed?
                if (($conf & CustomProperty::ON_EXPORT) === CustomProperty::ON_EXPORT && ($conf & $mode) === $mode)
                {
                    $value = $this->exportCustomProperty($property, $value, $rawDataMode, $fields, $excluding, $ignoreEmptyValues);
                    // ignore null values
                    if ( ! $ignoreEmptyValues || ! empty($value) || $value === false || $value === 0)
                    {
                        $result->$key = $value;
                    }
                    // ok, go to next property
                    continue;
                }
            }
            ////////// DEPRECATED //////////////
            // the current key should be handled specially?
            elseif (in_array($key, $this->propertyExceptions))
            {
                $value = $this->serializingSpecialProperty($property, $value, $rawDataMode);
                // ignore null values
                if ( ! $ignoreEmptyValues || ! empty($value) || $value === false || $value === 0)
                {
                    $result->$key = $value;
                }
                // ok, go to next property
                continue;
            }
            ////////// END DEPRECATED //////////////
            // ignore null values
            if ($ignoreEmptyValues && empty($value) && $value !== false && $value !== 0)
            {
                continue;
            }
            // we are processing the ids?
            if ($rawDataMode && $property === static::$ivar_publicId)
            {
                if ( ! empty($value))
                {
                    $key = $this->get_ivar_internalId();
                    // set the is key and value
                    $result->$key = $this->idToRawData($value);
                }
                // ok, go to next property
                continue;
            }
            // get the child-fields for this property
            $subFields = isset($fields[$property]) && is_array($fields[$property]) ? $fields[$property] : [];
            // the current value is an object??
            if (is_object($value))
            {
                if ($value instanceof \DateTime)
                {
                    $result->$key = $rawDataMode ? $this->dateTimeToRawData($value) : $value->getTimestamp()*self::$TimestampFactor;
                }
                elseif ($value instanceof Serializable)
                {
                    if ($rawDataMode && isset($this->properties[$property][3]))
                    {
                        $id = $value->get_ivar_publicId();
                        // convert the ID to RawData Id
                        $result->$key = $value->idToRawData($value->$id);
                    }
                    else // serialize and assign the object
                    {
                        $result->$key = $value->exportProperties($subFields, $excluding, $ignoreEmptyValues, $rawDataMode);
                    }
                }
                elseif ($value instanceof stdClass)
                {
                    $result->$key = $value;
                }
            }
            elseif (is_array($value) && $this->properties[$property][1] === Property::IS_ARRAY && isset($this->properties[$property][2]))
            {
                $array = [];
                // serialize each object element
                foreach ($value as $item)
                {
                    if (is_object($item) && $item instanceof Serializable)
                    {
                        // this property has the CREATE_ON_SERIALIZE_BY_ID or CREATE_ON_DEMAND_BY_ID defined so the id should be returned instead
                        if ($rawDataMode && isset($this->properties[$property][3]))
                        {
                            $id = $item->get_ivar_publicId();
                            // convert the ID to RawData Id
                            $array[] = $item->idToRawData($item->$id);
                        }
                        else // standard object
                        {
                            $array[] = $item->exportProperties($subFields, $excluding, $ignoreEmptyValues, $rawDataMode);
                        }
                    }
                    else // error, is not a serializable object
                    {
                        throw new SerializeIsNotSubclassOfSerializableException(is_object($item) ? get_class($item) : 'NULL', $this);
                    }
                }
                // ok, set the array
                $result->$key = $array;
            }
            elseif (is_array($value) && $this->properties[$property][1] === Property::IS_MIXED)
            {
                $array = [];
                // try to serialize each
                foreach ($value as $item)
                {
                    // is this value an instance of Serializable?
                    if ($item instanceof Serializable)
                    {
                        $array[] = $item->exportProperties($subFields, $excluding, $ignoreEmptyValues, $rawDataMode);
                    }
                    else // is not serializable object, so return it as is
                    {
                        $array[] = $item;
                    }
                }
                // ok, set the array
                $result->$key = $array;
            }
            elseif ($this->properties[$property][1] === Property::IS_STRING_ENCRYPTED)
            {
                // for json mode we need to get the "raw" value ($this->ivars[$property]) because "$value" contains the decrypted value
                // when is "rawDataMode" the $value contains the encrypted value
                $result->$key = $rawDataMode ? $this->binaryDataToRawData($value) : base64_encode($this->ivars[$property]);
            }
            elseif ($this->properties[$property][1] === Property::IS_BINARY)
            {
                $result->$key = $rawDataMode ? $this->binaryDataToRawData($value) : base64_encode($value);
            }
            else // standard value
            {
                $result->$key = $value;
            }
        }
        // the raw data
        return $result;
    }

    /**
     * Assigna les propietats de l'objecte a partir d'un json.
     *
     * @param array|stdClass $data La informació del qual es trauran els properties.
     * @param boolean $replace TRUE = Els properties no definits al json s'esborraran, FALSE = els properties no definits al json es mantindran a l'objecte (si aquests estan definits).
     * @param boolean $rawDataMode TRUE = Converteix els IDs (si és necessari) i altres conversions, FALSE = els ids els tracta com properties "normals".
     * @param boolean $writableOnly TRUE = Només els properties que són READ_WRITE o WRITE_ONLY són els que s'assignen (utilitzant els setters), la resta no.
     *
     * @throws
     */
    protected function importProperties($data, bool $replace = true, bool $rawDataMode = false, bool $writableOnly = false): void
    {
        if ( ! $data)
        {
            return;
        }
        // set as new object
        if ($replace)
        {
            $this->ivars = [];
        }
        // writable properties only?
        $writableProperties = $writableOnly ? $this->writableProperties() : null;
        // is an stdClass? then convert it to array
        if (is_object($data))
        {
            $data = get_object_vars($data);
        }
        // ensure data is an array
        if ( ! is_array($data)) throw new SerializeInvalidObjectDataException($this);
        // iterate over all data keys
        foreach ($data as $property => $value)
        {
            if ($writableOnly && ! in_array($property, $writableProperties))
            {
                continue;
            }
            // the current key should be handled specially?
            if (array_key_exists($property, $this->customProperties))
            {
                $conf = $this->customProperties[$property];
                $mode = $rawDataMode ? CustomProperty::ON_RAW_MODE : CustomProperty::ON_JSON_MODE;
                // should be it processed?
                if (($conf & CustomProperty::ON_IMPORT) === CustomProperty::ON_IMPORT && ($conf & $mode) === $mode)
                {
                    $this->set_ivar($property, $this->importCustomProperty($property, $value, $replace, $rawDataMode, $writableOnly));
                    // go to next property
                    continue;
                }
            }
            // deprecated version of property exceptions (will disappear in next major version)
            if (in_array($property, $this->propertyExceptions))
            {
                $this->set_ivar($property, $this->unserializingSpecialProperty($property, $value, $rawDataMode));
            }
            // is the id key?
            elseif ($rawDataMode && $property === $this->get_ivar_internalId())
            {
                $this->set_ivar($this->get_ivar_publicId(), $this->idFromRawData($value));
            }
            // is a valid property?
            elseif ($this->hasProperty($property))
            {
                if ($value === null)
                {
                    // is an on-demand property?
                    if (isset($this->properties[$property][3]))
                    {
                        unset($this->ivarsOnDemand[$property]);
                    }
                    // set as null the property
                    $this->ivars[$property] = null;
                    // continue to next one
                    continue;
                }
                // is a date property?
                if ($this->properties[$property][1] === Property::IS_DATE)
                {
                    $value = $rawDataMode ? $this->dateTimeFromRawData($value) : new DateTime('@'.(int)($value/self::$TimestampFactor));
                }
                // is a binary property?
                elseif ($this->properties[$property][1] === Property::IS_BINARY)
                {
                    $value = $rawDataMode ? $this->binaryDataFromRawData($value) : base64_decode($value);
                }
                // is an encrypted string property?
                elseif ($this->properties[$property][1] === Property::IS_STRING_ENCRYPTED)
                {
                    $this->ivars[$property] = $rawDataMode ? $this->binaryDataFromRawData($value) : base64_decode($value);
                    // go next...
                    continue;
                }
                else // standard ivar
                {
                    // determine the class factory (rawData or json)
                    $factory = $rawDataMode ? 'instanceWithRawData' : 'instanceWithJson';
                    // is an object property?
                    if ($this->properties[$property][1] === Property::IS_OBJECT || $this->properties[$property][1] === Property::IS_OBJECT_NULLABLE)
                    {
                        /** @var string|Serializable $class */
                        $class = static::guessClassName($value, array_merge([$property], $this->properties[$property]), $this);
                        // is a lazy property
                        if (isset($this->properties[$property][3]))
                        {
                            // store the "on-demand" id/data
                            $this->ivarsOnDemand[$property] = $class::parseDataBeforeInsertInIvarsOnDemand($value);
                            // should load now the property?
                            if ($this->properties[$property][3] === SerializableProperty::CREATE_ON_SERIALIZE_BY_ID)
                            {
                                $value = $this->__get($property);
                            }
                            elseif ($this->properties[$property][3] === SerializableProperty::CREATE_ON_DEMAND_BY_ID)
                            {
                                continue;
                            }
                        }
                        else // standard object serialization
                        {
                            /** @var Serializable $currentValue */
                            $currentValue = $this->ivars[$property] ?? null;
                            // instantiate the object
                            if ($replace || $currentValue === null || get_class($currentValue) !== $class)
                            {
                                $value = $class::$factory($value, $replace, $writableOnly);
                            }
                            else // reuse the current instance
                            {
                                $currentValue->importProperties($value, $replace, $rawDataMode, $writableOnly);
                                // the value is the "current value" but with updates properties
                                $value = $currentValue;
                            }
                        }
                    }
                    // is an array
                    elseif ($this->properties[$property][1] === Property::IS_ARRAY && is_array($value) && isset($this->properties[$property][2]))
                    {
                        if (isset($this->properties[$property][3]))
                        {
                            $this->ivarsOnDemand[$property] = [];
                            // parse each on-demand ivar
                            foreach ($value as $infoRawData)
                            {
                                /** @var string|Serializable $class */
                                $class = static::guessClassName($infoRawData, array_merge([$property], $this->properties[$property]), $this);
                                // add the on-demand ivar
                                $this->ivarsOnDemand[$property][] = $class::parseDataBeforeInsertInIvarsOnDemand($infoRawData);
                            }
                            // should load now the property?
                            if ($this->properties[$property][3] === SerializableProperty::CREATE_ON_SERIALIZE_BY_ID)
                            {
                                $value = $this->__get($property);
                            }
                            elseif ($this->properties[$property][3] === SerializableProperty::CREATE_ON_DEMAND_BY_ID)
                            {
                                continue;
                            }
                        }
                        else // standard array object serialization
                        {
                            $values = [];
                            // fill values with objects
                            foreach ($value as $infoRawData)
                            {
                                $class = static::guessClassName($infoRawData, array_merge([$property], $this->properties[$property]), $this);
                                // instantiate the object and add into the array
                                $values[] = $class::$factory($infoRawData, $replace, $writableOnly);
                            }
                            // value is now the array of objects
                            $value = $values;
                        }
                    }
                }
                // assign the value using the setter
                if ($writableOnly)
                {
                    $this->$property = $value;
                }
                else // assign the ivar directly
                {
                    $this->set_ivar($property, $value);
                }
            }
        }
    }

    /**
     * Importa un array d'un Json, però té en compte els Ids.
     *
     * @param string $property El property.
     * @param array|null $values El valor de l'array.
     * @param bool $replace Si ha de remplaçar o no.
     * @param bool $writableOnly Si només els properties d'escriptura.
     *
     * @return array|null
     */
    protected function importSafeArrayProperty(string $property, ?array $values, bool $replace, bool $writableOnly): ?array
    {
        if (is_array($values) && $this->properties[$property][1] === Property::IS_ARRAY && isset($this->properties[$property][2]))
        {
            $result = [];
            foreach ($values as $value)
            {
                /** @var static $class */
                $class = $this->properties[$property][2];
                $item = $class::instanceWithJson($value, $replace, $writableOnly);
                $valuePublicId = $class::$ivar_publicId;
                // verifies if this id is already exists
                $exists = array_filter($this->ivars[$property] ?? [], function ($el) use ($value, $valuePublicId)
                {
                    $elPublicId = $el->get_ivar_publicId();
                    // compare the ids
                    return $el->$elPublicId === $value->$valuePublicId;
                });
                // update the id
                $itemPublicId = $item->get_ivar_publicId();
                $item->set_ivar($itemPublicId, empty($exists) ? $item->$itemPublicId : $value->$valuePublicId);
                // add this new item
                $result[] = $item;
            }
            return $result;
        }
        return null;
    }

    private function get_ivar_publicId(): string
    {
        return static::$ivar_publicId;
    }

    // Public JSON methods

    private function get_ivar_internalId(): string
    {
        return static::$ivar_internalId;
    }

    /**
     * El mètode Getter de qualsevol dels objectes Serializable.
     *
     * @param string $name El nom del property que es vol obtenir.
     *
     * @return mixed El valor del property.
     *
     * @throws ObjectInvalidPropertyAliasException
     * @throws ObjectMissingPropertyException
     * @throws ObjectWriteOnlyException
     * @throws SerializableDontImplementsDefaultFactoryException
     * @throws SerializableUnsupportedOnDemandPropertyTypeException
     */
    public function __get(string $name)
    {
        if (isset($this->ivarsOnDemand[$name]) && $this->hasProperty($name))
        {
            /** @var Serializable|string $class */
            $class = $this->properties[$name][2];
            // is an array property?
            if ($this->properties[$name][1] === Property::IS_ARRAY)
            {
                $value = [];
                // serialize each array element
                foreach ($this->ivarsOnDemand[$name] as $item)
                {
                    $value[] = $class::instanceByRawDataId($item);
                }
            }
            elseif ($this->properties[$name][1] === Property::IS_OBJECT || $this->properties[$name][1] === Property::IS_OBJECT_NULLABLE)
            {
                $value = $class::instanceByRawDataId($this->ivarsOnDemand[$name]);
            }
            else // unsupported property on-demand
            {
                throw new SerializableUnsupportedOnDemandPropertyTypeException($class, $name);
            }
            // assign the loaded value
            $this->set_ivar($name, $value);
            // unset the on-demand ivar
            unset($this->ivarsOnDemand[$name]);
        }
        // standard behavior
        return parent::__get($name);
    }

    /**
     * Assigna les propietats de l'objecte a partir d'un json.
     *
     * @param array|stdClass $json El json del qual es trauran els properties.
     * @param boolean $replace TRUE = Els properties no definits al json s'esborraran, FALSE = els properties no definits al json es mantindran a l'objecte (si aquests estan definits).
     * @param boolean $writableOnly TRUE = Només els properties que són READ_WRITE o WRITE_ONLY són els que s'assignen (utilitzant els setters), la resta no.
     *
     * @throws
     */
    public function fromJson($json, bool $replace = true, bool $writableOnly = false): void
    {
        $this->importProperties($json, $replace, false, $writableOnly);
    }

    // Public RawData methods

    /**
     * Converteix un objecte de forma recursiva a json (stdClass)
     *
     * @return stdClass L'objecte en format json.
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    public function json(?array $fields = null, bool $excluding = true): stdClass
    {
        return $this->exportProperties($fields, $excluding);
    }

    /**
     * Assigna les propietats de l'objecte a partir d'el raw data.
     *
     * @param array|stdClass $rawData La info de la qual es trauran els properties.
     * @param boolean $replace TRUE = Els properties no definits al rawData s'esborraran, FALSE = els properties no definits al rawData es mantindran a l'objecte (si aquests estan definits).
     * @param boolean $writableOnly TRUE = Només els properties que són READ_WRITE o WRITE_ONLY són els que s'assignen (utilitzant els seus setters), la resta no.
     *
     * @throws
     */
    public function fromRawData($rawData, bool $replace = true, bool $writableOnly = false): void
    {
        $this->importProperties($rawData, $replace, true, $writableOnly);
    }

    /**
     * Converteix els properties a serializables, es a dir, preparats per a ser guardats a una BD.
     *
     * @param boolean $array TRUE = Retorna el rawData en format array, FALSE = en format stdClass (per defecte).
     *
     * @return stdClass|array Els properties serialitzats.
     *
     * @throws
     */
    public function rawData(bool $array = false)
    {
        $result = $this->exportProperties([], false, false, true);
        // should convert the rawData to array?
        return $array ? Helpers::stdClassToArray($result) : $result;
    }

    /**
     * Retorna una copia recursiva de l'objecte.
     *
     * @return static La còpia de l'objecte.
     *
     * @throws
     */
    public function copy(): self
    {
        return static::instanceWithRawData($this->rawData());
    }

    // Serializable Interface implementation

    /**
     * PHP 7.3 or lower -- stdClass or String representation of object.
     *
     * @link https://php.net/manual/en/serializable.serialize.php
     *
     * @param bool $stringify Return the JSON representation as string
     *
     * @return stdClass|string The stdClass or string representation of the object or null
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     *
     * @since 5.1.0
     */
    public function serialize($stringify = false)
    {
        return $stringify ? json_encode($this) : $this->json();
    }

    /**
     * PHP 7.3 or lower -- Constructs the object
     *
     * @link https://php.net/manual/en/serializable.unserialize.php
     *
     * @param mixed $serialized The representation of the object.
     *
     * @return void
     *
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        if (is_string($serialized)) $serialized = json_decode($serialized);
        // load data from a json
        $this->fromJson($serialized);
    }

    /**
     * PHP 7.4 or higher -- stdClass or String representation of object.
     *
     * @link https://php.net/manual/en/serializable.serialize.php
     *
     * @return stdClass|string The stdClass or string representation of the object or null
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    public function __serialize()
    {
        return $this->serialize(false);
    }

    /**
     * PHP 7.4 or higher -- Constructs the object
     *
     * @link https://php.net/manual/en/serializable.unserialize.php
     *
     * @param mixed $data The representation of the object.
     *
     * @return void
     */
    public function __unserialize($data): void
    {
        $this->unserialize($data);
    }

    // JsonSerializable Interface implementation

    /**
     * Specify data which should be serialized to JSON
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     *
     * @since 5.4.0
     *
     */
    public function jsonSerialize(): stdClass
    {
        return $this->json();
    }
}
