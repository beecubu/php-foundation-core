<?php

namespace Beecubu\Foundation\Core;

use Exception;
use ReflectionClass;

/**
 * Defineix un tipus Enum.
 */
abstract class Enum
{
    static private $values = [];

    /**
     * Comprova si un valor és un enum vàlid.
     *
     * @param string|int|null $value El valor a comprovar.
     * @param bool $nullable TRUE = El valor pot ser null, FALSE = el valor NO pot ser null.
     *
     * @return boolean TRUE = És un valor vàlid per aquest enum, FALSE = no ho és.
     */
    public static function validateValue($value, bool $nullable = false): bool
    {
        $result = false;
        // has values to check?
        if ($values = static::values())
        {
            $result = in_array($value, self::$values[static::class]);
        }
        // is null a valid value?
        return $result || $value === null && $nullable;
    }

    /**
     * Comprova si cada un dels valors de l'array és un enum vàlid.
     *
     * @param array $values Els valors a comprovar.
     * @param array $invalid Els elements que no són vàlids (si n'hi han).
     *
     * @return boolean TRUE = Son valors vàlids per aquest enum, FALSE = no ho són.
     */
    public static function validateValues(array $values, ?array &$invalid): bool
    {
        $invalid = [];
        // validate each element
        foreach ($values as $value)
        {
            if ( ! static::validateValue($value))
            {
                $invalid[] = $value;
            }
        }
        // is there any invalid element?
        return count($invalid) === 0;
    }

    /**
     * Retorna els valors de l'enum.
     *
     * @return array|null Els valors de l'enum.
     */
    public static function values(): ?array
    {
        if ( ! isset(self::$values[static::class]))
        {
            try
            {
                self::$values[static::class] = array_values((new ReflectionClass(static::class))->getConstants());
            }
            catch (Exception $exception)
            {
                return null;
            }
        }
        // return the enum values
        return self::$values[static::class];
    }
}
